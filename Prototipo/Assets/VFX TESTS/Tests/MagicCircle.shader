﻿Shader "Unlit/MagicCircle"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_CircleRadius("Radius",Range(0,1)) = 0
		_CircleThickness("Thickness",Range(0,1)) = 0
		_CircleSmmoothness("Smoothness",Range(0,0.25)) = 0.05
		_CircleAlpha("Alpha",Range(0,1)) = 0
		[HDR]_CircleColor("Color",color) = (1.0,1.0,1.0,1.0)
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
        LOD 100
		ZWrite Off
		Blend One One
		Cull Off

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST, _CircleColor;
			float _CircleRadius, _CircleThickness, _CircleAlpha, _CircleSmmoothness;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				//Custom Vertex Data;
				o.uv.zw = v.uv.zw;
				//End
                UNITY_TRANSFER_FOG(o,o.vertex);
				o.color = v.color;
                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				float2 center = float2(0.5,0.5);


                //fixed4 col = tex2D(_MainTex, i.uv);
				fixed4 col = smoothstep(_CircleRadius - _CircleSmmoothness,_CircleRadius,distance(center, i.uv)) * smoothstep(distance(center, i.uv)- _CircleSmmoothness,distance(center, i.uv), _CircleThickness +_CircleRadius);
                return col * _CircleColor * (i.color*i.color.a) * (i.uv.z+ pow(_CircleAlpha, 3)) ;
            }
            ENDCG
        }
    }
}
