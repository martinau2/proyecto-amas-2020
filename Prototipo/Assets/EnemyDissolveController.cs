﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDissolveController : MonoBehaviour
{
    public GameObject _MaterialObject;
    public float _Speed;
    Material material;
    float currentval;

    // Start is called before the first frame update
    void Start()
    {
        material = _MaterialObject.GetComponent<SkinnedMeshRenderer>().material;
    }

    [ContextMenu("Desintegrate")]
    public void Desintegrate()
    {
        StartCoroutine(LerpTo1());
    }

    [ContextMenu("Reintegrate")]
    public void Reintegrate()
    {
        StartCoroutine(LerpTo0());
    }


    public IEnumerator LerpTo1()
    {
        currentval = 0;
        while (currentval < 1)
        {
            currentval += Time.deltaTime* _Speed;
            currentval = Mathf.Clamp01(currentval);
            material.SetFloat("_DisAmount", currentval);
            yield return null;
        }
    }

    public IEnumerator LerpTo0()
    {
        currentval = 1;
        while (currentval > 0)
        {
            currentval -= Time.deltaTime* _Speed;
            currentval = Mathf.Clamp01(currentval);
            material.SetFloat("_DisAmount", currentval);
            yield return null;
        }
    }
}
