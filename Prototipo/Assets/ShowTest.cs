﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowTest : MonoBehaviour
{
    public GameObject spell1;

    public GameObject slot1;
    public GameObject slot2;
    public GameObject slot3;
    public GameObject slot4;
    public GameObject slot5;
    public GameObject slot6;
    public GameObject slot7;
    public GameObject slot8;
    public GameObject slot9;
    public GameObject slot0;

    public GameObject firetorrentpos;

    public GameObject cam1;
    public GameObject cam2;
    

    private void Update()
    {
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            transform.LookAt(hit.point);
        }

        if (Input.GetKeyDown(KeyCode.F))
        {
            cam2.gameObject.SetActive(false);
            cam1.gameObject.SetActive(true);
        }
        if (Input.GetKeyDown(KeyCode.G))
        {
            cam1.gameObject.SetActive(false);
            cam2.gameObject.SetActive(true);
        }


        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            spell1 = slot1;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            spell1 = slot2;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha3))
        {
            spell1 = slot3;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha4))
        {
            spell1 = slot4;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha5))
        {
            transform.position = firetorrentpos.transform.position;
            spell1 = slot5;
            Instantiate(spell1, transform.position, Quaternion.identity);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            spell1 = slot6;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha7))
        {
            spell1 = slot7;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha8))
        {
            spell1 = slot8;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha9))
        {
            spell1 = slot9;
            Instantiate(spell1, transform.position, transform.rotation);
        }
        else if (Input.GetKeyDown(KeyCode.Alpha0))
        {
            spell1 = slot0;
            Instantiate(spell1, transform.position, transform.rotation);
        }
    }
}
