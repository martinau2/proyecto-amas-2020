﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatformPuzzleLv2 : MonoBehaviour
{
    Animation movingPlatformAnim;

    [SerializeField]
    GameObject antorcha;
    [SerializeField]
    GameObject fuegobrasero;
    [SerializeField]
    float secondsToWait;

    public string exitAnim;

    // Start is called before the first frame update
    void Start()
    {
        
        movingPlatformAnim = GetComponent<Animation>();
        movingPlatformAnim.wrapMode = WrapMode.Once;

    }

    // Update is called once per frame
    void Update()
    {
        if (fuegobrasero.activeSelf)
        {
            movingPlatformAnim.Play();
 //         
            Debug.Log("plataforma debio moverse");
            StartCoroutine("TimeUntilHidePlatform");
            
        }
       

    }

    IEnumerator TimeUntilHidePlatform()
    {
        yield return new WaitForSeconds(secondsToWait);
        fuegobrasero.SetActive(false);
        Debug.Log("Se tiene que apagar la antorcha");
        movingPlatformAnim.Play(exitAnim);
    }
}
