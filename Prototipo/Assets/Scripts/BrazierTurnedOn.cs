﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class BrazierTurnedOn : MonoBehaviour
{
    [SerializeField]
    public GameObject fuego;
    public UnityEvent reaccionar;



    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //private void OnCollisionEnter(Collision collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        Debug.Log("colisione con bola de fuego");
    //        if (!fuego.activeSelf)
    //        {
    //            fuego.SetActive(true);
    //            reaccionar.Invoke();
    //        }
            
    //    }
    //}

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "FireBall(Clone)")
        {
            Debug.Log("trigger con bola de fuego");
            fuego.SetActive(true);
            reaccionar.Invoke();
        }
    }
}
