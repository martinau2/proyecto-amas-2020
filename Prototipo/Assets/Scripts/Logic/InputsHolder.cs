﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputsHolder : MonoBehaviour
{

    public float _VerticalInput;
    public float _HorizontalInput;
    public bool _Jump;
    bool takeinput = true;

    // Update is called once per frame
    void Update()
    {
        
    }

    public void takeinputswitch(bool val)
    {
        SetVerticalInput(0);
        SetHorizontalInput(0);
        SetJumpInput(false);
        takeinput = val;
    }

    public void SetVerticalInput(float val)
    {
        if (!takeinput)
            return;
        _VerticalInput = val;
    }
    public void SetHorizontalInput(float val)
    {
        if (!takeinput)
            return;
        _HorizontalInput = val;
    }
    public void SetJumpInput(bool val)
    {
        if (!takeinput)
            return;
        _Jump = val;
    }
}
