﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageReciber : MonoBehaviour
{
    public UnityEventSpellInfo _InfoEvent;
    public float _CallWait;
    SpellInfo savedinfo;

    public void RaiseEvent(SpellInfo info)
    {
        savedinfo = info;
        Invoke("Raise", _CallWait);
    }

    void Raise()
    {
        _InfoEvent.Invoke(savedinfo);
    }
}
