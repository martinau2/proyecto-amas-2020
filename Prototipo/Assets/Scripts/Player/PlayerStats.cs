﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    public PlayerStatsInfo _Stats;

    public SerializableFloatEvent _UpdateHealth;
    public SerializableFloatEvent _UpdateStamina;

    private void Awake()
    {
        _Stats.ResetStats();
    }

    

    public void ChangeHP(float val)
    {
        _Stats.ChangeHealth(_Stats.health + val);
        _Stats.ChangeHealth(Mathf.Clamp(_Stats.health,0,_Stats._MaxHealth));

        //Debug.Log(_Stats.health.ToString("0.00HP"));

        if (_UpdateHealth == null) {return;}
        _UpdateHealth.Raise(_Stats.health/ _Stats._MaxHealth);
    }

    public void ChangeStamina(float val)
    {
        _Stats.ChangeStamina(_Stats.stamina + val);
        _Stats.ChangeStamina(Mathf.Clamp(_Stats.stamina, 0, _Stats._MaxStamina));

        //Debug.Log(_Stats.stamina.ToString("0.00SP"));

        if (_UpdateStamina == null) { return; }
        _UpdateStamina.Raise(_Stats.stamina/ _Stats._MaxStamina);
    }
}

[System.Serializable]
public class PlayerStatsInfo
{
    public float _MaxHealth;
    public float _MaxStamina;
    public float _MaxMana;

    public float health { get;private set; }
    public float stamina { get; private set; }
    public float mana { get; private set; }

    //public float GetMana() { return mana; }
    //public float GetHP() { return health; }
    //public float GetStamina() { return stamina; }

    public void ChangeHealth(float val) { health = val;}
    public void ChangeStamina(float val) { stamina = val;}
    public void ChangeMana(float val) { mana = val;}

    public void ResetStats()
    {
        health = _MaxHealth;
        stamina = _MaxStamina;
        mana = _MaxMana;
    }
}
