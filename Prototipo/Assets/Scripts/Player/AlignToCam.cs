﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlignToCam : MonoBehaviour
{

    public Transform _Character;
    public Transform _Camera;
    public Transform _Arms;
    public float _TurnSpeed;
    public float _ArmsTurnSpeed;
    float bodylerpprcnt;
    float armslerpprcnt;
    Vector3 targetfwd;
    Vector3 armsfwd;


    // Update is called once per frame
    private void Update()
    {
        bodylerpprcnt = Time.deltaTime * _TurnSpeed;
        armslerpprcnt = Time.deltaTime * _ArmsTurnSpeed;
    }


    void FixedUpdate()
    {

        

        

        float charlocaly = _Character.localEulerAngles.y;
        float camlocaly = _Camera.localEulerAngles.y;

        var angle = Mathf.LerpAngle(charlocaly, camlocaly, bodylerpprcnt);

        targetfwd = new Vector3(0, angle, 0);
        


        _Character.localEulerAngles = targetfwd;
        _Arms.forward = Vector3.Lerp(_Arms.forward , _Camera.forward , armslerpprcnt);
        


        

    }
    public void GoToForward()
    {
        _Character.localEulerAngles = targetfwd;
    }
}
