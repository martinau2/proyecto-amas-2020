﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class StateMachine
{
    public IState _CurrentState;
    public void ChangeState(IState newState)
    {
        if (_CurrentState != null)
            _CurrentState.Exit();

        _CurrentState = newState;
        _CurrentState.Enter();
    }

    public void Update()
    {
        if (_CurrentState != null)
        {
            _CurrentState.Update();
        }
        
    }

    public void InputUpdate()
    {
        if (_CurrentState != null)
        {
            _CurrentState.HandleInput();
        }
    }

    public void PhysicsUpdate()
    {
        if (_CurrentState != null)
        {
            _CurrentState.PhysicsExecute();
        }
    }


    public IState GetCurrentState() {
        return _CurrentState;

    }
}

public interface IState
{
    void Enter();
    void HandleInput();
    void Update();
    void PhysicsExecute();
    void Exit();
}