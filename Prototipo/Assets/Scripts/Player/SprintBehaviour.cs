﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SprintBehaviour : MonoBehaviour
{
    public Character _Char;
    public PlayerStats _PlayerStats;
    public float _SprintSpeed;
    public float _StamineLossPerSecond;
    public float _StamineGainPerSecond;

    public float _CD;
    public float currentCD;
    public bool oncd;

    public SerializableEvent _OnCD;
    public SerializableEvent _OffCD;


    float initialSpeed;

    void Start()
    {
        _PlayerStats = GetComponent<PlayerStats>();
        _Char = GetComponent<Character>();
        initialSpeed = _Char._Speed;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift) && _PlayerStats._Stats.stamina > Mathf.Abs(_StamineLossPerSecond) && !oncd)
        {
            _Char._Speed = _SprintSpeed;
            _PlayerStats.ChangeStamina(_StamineLossPerSecond);
        }
        else if (Input.GetKey(KeyCode.LeftShift) && _PlayerStats._Stats.stamina < Mathf.Abs(_StamineLossPerSecond) && !oncd)
        {
            Debug.Log("Enter CD Mode");
            oncd = true;
            if (_OnCD != null) { _OnCD.Raise(); }
            currentCD = _CD;
            _Char._Speed = initialSpeed;
            _PlayerStats.ChangeStamina(_StamineGainPerSecond);
        }
        else
        {
            _Char._Speed = initialSpeed;
            _PlayerStats.ChangeStamina(_StamineGainPerSecond);
        }

        if (currentCD <= 0)
        {
            oncd = false;
            if (_OffCD != null) { _OffCD.Raise(); }
            return;
        }

        currentCD -= Time.deltaTime;
    }
}
