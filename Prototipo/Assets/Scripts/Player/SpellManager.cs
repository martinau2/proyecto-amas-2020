﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellManager : MonoBehaviour
{
    public Rune[] _AllRunes;

    // Update is called once per frame
    void Update()
    {
        foreach (var rune in _AllRunes)
        {
            rune.ReduceCD();
        }
    }
}
