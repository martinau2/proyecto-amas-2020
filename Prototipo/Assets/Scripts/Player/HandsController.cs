﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum HandsAnimationStrings
{
    LeftHandIdle = 0,
    LeftHandHold = 1,
    LeftHandRelease = 2,
    RightHandIdle = 3,
    RightHandHold = 4,
    RightHandRelease = 5
}
public enum HandType
{
    Left = 0,
    Right = 1
}

public class HandsController : MonoBehaviour
{
    public GameObject _LeftHand;
    public StateController _LHSC;
    public GameObject _RightHand;
    public StateController _RHSC;
    public Animator _HandsAnimator;

    public DataFromTargetPos lhtargetpos;
    public DataFromTargetPos rhtargetpos;

    public Rune _LHRune;
    public Rune _RHRune;

    public GameObject _LHSpeelIndicator;
    public GameObject _RHSpeelIndicator;


    public void ChangeRune(bool lefthand,Rune rune)
    {
        if (lefthand)
        {
            _LHRune = rune;
            _LHSC._SM.ChangeState(new IdleHand(HandType.Left, this, _LHSC._SM));
        }
        else
        {
            _RHRune = rune;
            _RHSC._SM.ChangeState(new IdleHand(HandType.Right, this, _RHSC._SM));
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        _LHSC = _LeftHand.GetComponent<StateController>();
        _RHSC = _RightHand.GetComponent<StateController>();
        _LHSC._SM.ChangeState(new IdleHand(HandType.Left,this, _LHSC._SM));
        _RHSC._SM.ChangeState(new IdleHand(HandType.Right,this, _RHSC._SM));

        Cursor.visible = false;
    }
    
    void Update()
    {
        //_LHRune.ReduceCD();
        //_RHRune.ReduceCD();
    }

    public void TransitionWrapper(StateMachine statemachine, float timer, IState state)
    {
        StartCoroutine(TransitionToState(statemachine, timer, state));
    }

    public IEnumerator TransitionToState(StateMachine statemachine, float timer, IState state)
    {
        var x = timer;
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            yield return null;
        }
        statemachine.ChangeState(state);
    }

    [ContextMenu("Stop Hands")]
    public void SwitchHandPause(bool condition)
    {
        _LHSC.PauseStates(!condition);
        _RHSC.PauseStates(!condition);
    }
    

}

public class IdleHand : DefaultState
{
    public HandsController _Hands;
    bool left;
    ParticleSystem particle;
    bool transitioning = false;

    public IdleHand(HandType leftOrRight,HandsController hands,StateMachine stateMachine) : base(stateMachine)
    {
        _Hands = hands;
        left = leftOrRight == HandType.Left;
    }

    public override void Enter()
    {
        //Send Animator Trigger Info
        _Hands._HandsAnimator.SetTrigger(left ? HandsAnimationStrings.LeftHandIdle.ToString() : HandsAnimationStrings.RightHandIdle.ToString());
        //Instantiate particle effect
        particle = left ? _Hands._LHRune._RuneIdleParticle : _Hands._RHRune._RuneIdleParticle;
        particle = MonoBehaviour.Instantiate(particle, left ? _Hands._LeftHand.transform.position : _Hands._RightHand.transform.position, Quaternion.identity, left ? _Hands._LeftHand.transform : _Hands._RightHand.transform);
        
    }

    public override void Exit()
    {
        //Reset Trigger Just In Case
        _Hands._HandsAnimator.ResetTrigger(left ? HandsAnimationStrings.LeftHandIdle.ToString() : HandsAnimationStrings.RightHandIdle.ToString());
        //Reset Particle
        particle.loop = false;
        MonoBehaviour.Destroy(particle.gameObject,particle.main.duration);
        
    }

    public override void HandleInput()
    {
        if (transitioning)
        {
            return;
        }
        if (Input.GetKeyDown(KeyCode.Mouse0) && left && _Hands._LHRune.GetCD() == 0)
        {
            transitioning = true;
            _Hands.TransitionWrapper(stateMachine, _Hands._LHRune._RuneIdleParticleTimer, new HoldHand(HandType.Left, _Hands, stateMachine));
            //_Hands._LHSC._SM.ChangeState(new HoldHand(HandType.Left, _Hands, stateMachine));
        }
        else if (Input.GetKeyDown(KeyCode.Mouse1) && !left && _Hands._RHRune.GetCD() == 0) 
        {
            transitioning = true;
            _Hands.TransitionWrapper(stateMachine, _Hands._RHRune._RuneIdleParticleTimer, new HoldHand(HandType.Right, _Hands, stateMachine));
           // _Hands._RHSC._SM.ChangeState(new HoldHand(HandType.Right, _Hands, stateMachine));
        }


    }

    public override void PhysicsExecute()
    {
    }

    public override void Update()
    {
    }
}

public class HoldHand : DefaultState
{
    public HandsController _Hands;
    bool left;
    bool transitioning = false;
    ParticleSystem particle;

    public HoldHand(HandType leftOrRight, HandsController hands, StateMachine stateMachine) : base(stateMachine)
    {
        _Hands = hands;
        left = leftOrRight == HandType.Left;
    }

    public override void Enter()
    {
        //Active Spell Indicators
        if (left)
            _Hands._LHSpeelIndicator.SetActive(true);
        else 
            _Hands._RHSpeelIndicator.SetActive(true);

        //Instantiate particle effect
        particle = left ? _Hands._LHRune._RuneHoldParticle : _Hands._RHRune._RuneHoldParticle;
        particle = MonoBehaviour.Instantiate(particle, left ? _Hands._LeftHand.transform.position : _Hands._RightHand.transform.position, Quaternion.identity, left ? _Hands._LeftHand.transform : _Hands._RightHand.transform);
        //Send Animator Trigger Info
        _Hands._HandsAnimator.SetTrigger(left ? HandsAnimationStrings.LeftHandHold.ToString() : HandsAnimationStrings.RightHandHold.ToString());
    }

    public override void Exit()
    {
        if (left)
            _Hands._LHSpeelIndicator.SetActive(false);
        else
            _Hands._RHSpeelIndicator.SetActive(false);

        //Reset Trigger Just In Case
        _Hands._HandsAnimator.ResetTrigger(left ? HandsAnimationStrings.LeftHandHold.ToString() : HandsAnimationStrings.RightHandHold.ToString());
        //Reset Particle
        particle.loop = false;
        MonoBehaviour.Destroy(particle.gameObject, particle.main.duration);
        
    }

    public override void HandleInput()
    {
        if (transitioning)
        {
            return;
        }
        if (Input.GetKeyUp(KeyCode.Mouse0) && left)
        {
            transitioning = true;
            _Hands.TransitionWrapper(stateMachine, _Hands._LHRune._RuneHoldParticleTimer, new ReleaseHand(HandType.Left, _Hands, stateMachine));
            //_Hands._LHSC._SM.ChangeState(new ReleaseHand(HandType.Left, _Hands, stateMachine));
        }
        else if (Input.GetKeyUp(KeyCode.Mouse1) && !left)
        {
            transitioning = true;
            _Hands.TransitionWrapper(stateMachine, _Hands._RHRune._RuneHoldParticleTimer, new ReleaseHand(HandType.Right, _Hands, stateMachine));
            //_Hands._RHSC._SM.ChangeState(new ReleaseHand(HandType.Right, _Hands, stateMachine));
        }
    }

    public override void PhysicsExecute()
    {
    }

    public override void Update()
    {
        //Logic for spell placement
        if (left)
        {
            _Hands.lhtargetpos = _Hands._LHRune.OnHold(_Hands._LeftHand.transform);
            //Indicators
            _Hands._LHSpeelIndicator.transform.position = _Hands.lhtargetpos.indicatorpos;
            _Hands._LHSpeelIndicator.transform.up = _Hands.lhtargetpos.indicatorup;
        }
        else
        {
            _Hands.rhtargetpos = _Hands._RHRune.OnHold(_Hands._RightHand.transform);
            //Indicators
            _Hands._RHSpeelIndicator.transform.position = _Hands.rhtargetpos.indicatorpos;
            _Hands._RHSpeelIndicator.transform.up = _Hands.rhtargetpos.indicatorup;
        }
    }
}

public class ReleaseHand : DefaultState
{
    public HandsController _Hands;
    bool left;
    bool transitioning = false;
    ParticleSystem particle;

    public ReleaseHand(HandType leftOrRight, HandsController hands, StateMachine stateMachine) : base(stateMachine)
    {
        _Hands = hands;
        left = leftOrRight == HandType.Left;
    }

    public override void Enter()
    {
        //Send Animator Trigger Info
        _Hands._HandsAnimator.SetTrigger(left ? HandsAnimationStrings.LeftHandRelease.ToString() : HandsAnimationStrings.RightHandRelease.ToString());

        //Instantiate particle effect
        particle = left ? _Hands._LHRune._RuneReleaseParticle : _Hands._RHRune._RuneReleaseParticle;
        particle = MonoBehaviour.Instantiate(particle, left ? _Hands._LeftHand.transform.position : _Hands._RightHand.transform.position, left ? _Hands._LeftHand.transform.rotation : _Hands._RightHand.transform.rotation, left ? _Hands._LeftHand.transform : _Hands._RightHand.transform);

        //Send OnRelease Logic and transition back to idle
        if (left)
        {
            _Hands._LHRune.OnRelease(_Hands._LeftHand.transform, _Hands.lhtargetpos);
            _Hands.TransitionWrapper(stateMachine, _Hands._LHRune._RuneReleaseParticleTimer, new IdleHand(HandType.Left, _Hands, stateMachine));
            //_Hands._LHSC._SM.ChangeState(new IdleHand(HandType.Left, _Hands, stateMachine));
        }
        else
        {
            _Hands._RHRune.OnRelease(_Hands._RightHand.transform, _Hands.rhtargetpos);
            _Hands.TransitionWrapper(stateMachine, _Hands._RHRune._RuneReleaseParticleTimer, new IdleHand(HandType.Right, _Hands, stateMachine));
            //_Hands._RHSC._SM.ChangeState(new IdleHand(HandType.Right, _Hands, stateMachine));
        }
    }

    public override void Exit()
    {
        //Reset Trigger Just In Case
        _Hands._HandsAnimator.ResetTrigger(left ? HandsAnimationStrings.LeftHandRelease.ToString() : HandsAnimationStrings.RightHandRelease.ToString());
        //Reset Particle
        particle.loop = false;
        MonoBehaviour.Destroy(particle.gameObject, particle.main.duration);
    }

    public override void HandleInput()
    {
    }

    public override void PhysicsExecute()
    {
    }

    public override void Update()
    {
    }
}


