﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;
using UnityEngine.SceneManagement;

public class DieWin : MonoBehaviour
{
    [SerializeField]
    GameObject panelUI;
    [SerializeField]
    Text textDieOrWin;

    [SerializeField]
    int sceneIndexToLoadafterWin;

    

    public void Die()
    {
        panelUI.SetActive(true);
        textDieOrWin.text = "You DIED";
        StartCoroutine("WaitTime");
        
        
    }

    public void Win()
    {
        panelUI.SetActive(true);
        textDieOrWin.text = "You WIN";
        StartCoroutine("WaitTime");
        SceneManager.LoadScene(sceneIndexToLoadafterWin);


    }

    IEnumerator WaitTime()
    {
        yield return new WaitForSeconds(5);
        //SceneManager.LoadScene(0);
    }
}
