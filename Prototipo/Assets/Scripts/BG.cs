﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BG : MonoBehaviour
{
    Color alpha;

    // Start is called before the first frame update
    void Start()
    {
        alpha = gameObject.GetComponent<SpriteRenderer>().color;
    }

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(0, 0, 5*Time.deltaTime);
        alpha.a = Random.Range(90f, 255f)*Time.deltaTime;
        //Debug.Log(alpha);
    }
}
