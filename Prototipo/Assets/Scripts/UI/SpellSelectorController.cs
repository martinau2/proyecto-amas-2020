﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SpellSelectorController : MonoBehaviour
{
    public HandsController _MainController;

    public ScriptableBoolEvent _OnUIActivation;
    public SerializableEvent _OnActivation;
    public SerializableEvent _OnDeactivation;

    bool uistate;

    public HandType _HType;

    public GameObject _HandSelector;
    public GameObject _SpellSelector;
    public TextMeshProUGUI _HandText;


    public void SetHandType(bool isleft)
    {
        if (isleft)
        {
            _HandText.text = "Left Hand";
            _HType = (HandType)0;
        }
        else
        {
            _HandText.text = "Right Hand";
            _HType = (HandType)1;
        }
    }

    private void Awake()
    {
        _MainController = FindObjectOfType<HandsController>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            SetHandType(true);
            SwitchUI();
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            SetHandType(false);
            SwitchUI();
        }
    }

    private void SwitchUI()
    {
        if (!uistate)
        {
            ActivateUI();
        }
        else
        {
            DeactivateUI();
        }
    }

    public void DeactivateUI()
    {
        Cursor.visible = !Cursor.visible;
        uistate = !uistate;
        _OnUIActivation.Raise(true);
        _OnDeactivation.Raise();
    }
    public void ActivateUI()
    {
        Cursor.visible = !Cursor.visible;
        uistate = !uistate;
        _OnUIActivation.Raise(false);
        _OnActivation.Raise();
    }


}
