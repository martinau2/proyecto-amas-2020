﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RuneInfoUI : MonoBehaviour
{

    public Rune _LHRuneType, _RHRuneType;
    public SpellSelectorController _Controller;
    public Button _UIButton;
    Image img;
    bool active = false;

    private void Awake()
    {
        _UIButton = GetComponent<Button>();

        img = _UIButton.GetComponent<Image>();
    }

    void Update()
    {
        //_LHRuneType.ReduceCD();
        //_RHRuneType.ReduceCD();

        if (_Controller._HType == HandType.Left)
        {
            var fill = _LHRuneType.GetCD() / _LHRuneType._ActionCD;
            img.fillAmount = 1  - fill;
        }
        else
        {
            var fill = _RHRuneType.GetCD() / _RHRuneType._ActionCD;
            img.fillAmount = 1 - fill;
        }


        if (!active)
        {
            return;
        }
        if ((_Controller._HType == HandType.Left ? _LHRuneType.GetCD() : _RHRuneType.GetCD()) != 0)
        {
            //Debug.Log("CD ACTIVE" + (_Controller._HType == HandType.Left ? _LHRuneType.GetCD() : _RHRuneType.GetCD()));
            _UIButton.interactable = false;
        }
        else
        {
            //Debug.Log("CD INACTIVE");
           _UIButton.interactable = true;
        }
    }

    public void setactive()
    {
        active = true;
    }
    public void deactive()
    {
        active = false;
    }
    

    public void SetRune()
    {
        if (_Controller._HType == HandType.Left)
        {
            _Controller._MainController.ChangeRune(true,_LHRuneType);
        }
        else
        {
            _Controller._MainController.ChangeRune(false,_RHRuneType);
        }
    }
}
