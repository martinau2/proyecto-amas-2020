﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StaminaState : MonoBehaviour
{
    public Color _ActiveStateColor;
    public Color _CDStateColor;

    Image _StaminaImage;

    private void Awake()
    {
        _StaminaImage = GetComponent<Image>();
    }

    public void OnActiveState()
    {
        _StaminaImage.color = _ActiveStateColor;
    }

    public void OnCDState()
    {
        _StaminaImage.color = _CDStateColor;
    }
}
