﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Gap : MonoBehaviour
{
    public UnityEvent playerDie;

    
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            playerDie.Invoke();

        }
    }
}
