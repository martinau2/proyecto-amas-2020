﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public struct timeandstep
{
    public float time;
    public int steps;
}

public enum TwiningEaseType {
    _Linear = 0,
    _EaseInQuad = 1,
    _EaseOutQuad = 2,
    _EaseOutSine = 3,
    _EaseInSine = 4,
    _EaseInOutSine = 5
}

public enum TwiningTimeType {
    _Yoyo = 0,
    _Linear = 1,
    _Steps = 2,
    _StepsYoyo = 3,
}

public static class Twining
{
    #region commentedout
    //
    //public Transform _ObjectToManipulate;
    //public Camera _Main;

    //public TwiningEaseType _Ease;
    //public TwiningTimeType _Time;

    //public float _Start;
    //public float _End;

    //public float _LoopTimer;
    //float middletimer;

    //public int _Steps;

    //SpriteRenderer renderer;


    //// Start is called before the first frame update
    //void Start()
    //{
    //    _ObjectToManipulate.position = _Main.ViewportToWorldPoint(new Vector3(0, 0, 0));
    //    renderer = _ObjectToManipulate.GetComponent<SpriteRenderer>();

    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    float percent = 0;
    //    if (_Time == TwiningTimeType._Yoyo)
    //    {
    //        _LoopTimer = (Mathf.Sin(Time.time - 1.5f) * 0.5f) + 0.5f;
    //    }
    //    if (_Time == TwiningTimeType._Linear)
    //    {
    //        _LoopTimer += Time.deltaTime;
    //        _LoopTimer = Mathf.Clamp01(_LoopTimer);

    //    }
    //    if (_Time == TwiningTimeType._Steps)
    //    {
    //        if (_Steps == 0)
    //        {
    //            _Steps = 1;
    //        }
    //        middletimer += Time.deltaTime;
    //        _LoopTimer = Mathf.Clamp01(middletimer);
    //        _LoopTimer *= _Steps;
    //        _LoopTimer = Mathf.Floor(_LoopTimer);
    //        _LoopTimer /= _Steps;

    //    }
    //    if (_Time == TwiningTimeType._StepsYoyo)
    //    {
    //        if (_Steps == 0)
    //        {
    //            _Steps = 1;
    //        }
    //        middletimer = (Mathf.Sin(Time.time - 1.5f) * 0.5f) + 0.5f;
    //        _LoopTimer = Mathf.Clamp01(middletimer);
    //        _LoopTimer *= _Steps;
    //        _LoopTimer = Mathf.RoundToInt(_LoopTimer);
    //        _LoopTimer /= _Steps;

    //    }


    //    if (_Ease == TwiningEaseType._Linear)
    //    {
    //        percent = Linear(_LoopTimer, _Start, _End);
    //    }
    //    if (_Ease == TwiningEaseType._EaseInQuad)
    //    {
    //        percent = EaseInQuad(_LoopTimer, _Start, _End);
    //    }
    //    if (_Ease == TwiningEaseType._EaseOutQuad)
    //    {
    //        percent = EaseOutQuad(_LoopTimer, _Start, _End);
    //    }
    //    if (_Ease == TwiningEaseType._EaseOutSine)
    //    {
    //        percent = EaseOutSine(_LoopTimer, _Start, _End);
    //    }
    //    if (_Ease == TwiningEaseType._EaseInSine)
    //    {
    //        percent = EaseInSine(_LoopTimer, _Start, _End);
    //    }
    //    if (_Ease == TwiningEaseType._EaseInOutSine)
    //    {
    //        percent = EaseInOutSine(_LoopTimer, _Start, _End);
    //    }


    //    _ObjectToManipulate.position = _Main.ViewportToWorldPoint(new Vector3(_LoopTimer, percent, 10));

    //    renderer.color = new Color(percent, 1, 0);



    //}
    //
    #endregion



    private static Dictionary<TwiningTimeType, Func<timeandstep, float>> TwiningTimerTypes = new Dictionary<TwiningTimeType, Func<timeandstep, float>>() {
        { TwiningTimeType._Linear,  new Func<timeandstep, float>(LinearTime) },
        { TwiningTimeType._Yoyo,  new Func<timeandstep, float>(LinearYoyo) },
        { TwiningTimeType._Steps,  new Func<timeandstep, float>(Steps) },
        { TwiningTimeType._StepsYoyo,  new Func<timeandstep, float>(YoyoSteps) }



    };

    public static float LinearTime(timeandstep time) {
        return Mathf.Clamp01(time.time);
    }
    private static float LinearYoyo(timeandstep time)
    {
        var timer = (Mathf.Sin(time.time - 1.5f) * 0.5f) + 0.5f;
        return timer;
    }
    private static float Steps(timeandstep timestep)
    {
        if (timestep.steps == 0)
        {
            timestep.steps = 2;
        }
        var timer = Mathf.Clamp01(timestep.time);
        timer *= timestep.steps;
        timer = Mathf.Floor(timer);
        timer /= timestep.steps;
        return timer;
    }
    private static float YoyoSteps(timeandstep timestep)
    {
        if (timestep.steps == 0)
        {
            timestep.steps = 2;
        }
        var timer = (Mathf.Sin(timestep.time - 1.5f) * 0.5f) + 0.5f;
        timer = Mathf.Clamp01(timer);
        timer *= timestep.steps;
        timer = Mathf.Floor(timer);
        timer /= timestep.steps;
        return timer;
    }


    public static float Linear(float time, float start, float end, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0) {
        time = ApplyTimeFunction(timetype, time, steps);
        return start + (end - start) * time;

    }
    public static float EaseInQuad(float time, float start, float end, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0)
    {
        time = ApplyTimeFunction(timetype, time, steps);
        return start + (end - start) * (time * time);
    }
    public static float EaseOutQuad(float time, float start, float end, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0)
    {
        time = ApplyTimeFunction(timetype, time, steps);
        return start + (end - start) * (1 - (1 - time) * (1 - time));
    }
    public static float EaseOutSine(float time, float start, float end, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0)
    {
        time = ApplyTimeFunction(timetype, time, steps);
        return start + (end - start) * (Mathf.Sin(time * Mathf.PI / 2));
    }
    public static float EaseInSine(float time, float start, float end, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0)
    {
        time = ApplyTimeFunction(timetype, time, steps);
        return start + (end - start) * (1 - Mathf.Cos((time * Mathf.PI) / 2));
    }
    public static float EaseInOutSine(float time, float start, float end, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0)
    {
        time = ApplyTimeFunction(timetype, time, steps);
        return start + (end - start) * -((Mathf.Cos(Mathf.PI * time) - 1) / 2);
    }
    public static float CustomCurve(float time, float start, float end, AnimationCurve curve, TwiningTimeType timetype = TwiningTimeType._Linear, int steps = 0)
    {
        time = ApplyTimeFunction(timetype, time, steps);
        return curve.Evaluate(time);
    }

    private static float ApplyTimeFunction(TwiningTimeType timetype, float time, int steps)
    {
        timeandstep timeandstep = new timeandstep();
        timeandstep.time = time;
        timeandstep.steps = steps;
        return  TwiningTimerTypes[timetype](timeandstep);


    }

}
