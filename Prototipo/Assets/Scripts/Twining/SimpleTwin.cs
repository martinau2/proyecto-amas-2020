﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleTwin : MonoBehaviour
{
    public TwiningTimeType _Type = TwiningTimeType._Yoyo;
    public float _Speed = 1.0f;
    public float _InitialVal;
    public float _FinalVal;
    public bool _UseCurve;
    public AnimationCurve _Curve;
    [Range(0,1)]public float _FromValue;
    [Range(0,1)] public float _ToValue;
    //public SerializableFloatEvent _ValueEvent;
    public UnityEventFloat _ValueEvent;
    float prct;
    float timer;

    bool event1 = false;
    bool event0 = true;

    public SerializableEvent _EventOn1;
    public SerializableEvent _EventOn0;
    

    private void Update()
    {
        if (prct == 1 && !event1)
        {
            _EventOn1?.Raise();
            event1 = true;
            event0 = false;
        }

        if (prct == 0 && !event0)
        {
            _EventOn0?.Raise();
            event0 = true;
            event1 = false;
        }

        timer += Time.deltaTime;
        if (_UseCurve)
        {
            prct = Twining.CustomCurve(timer * _Speed, _FromValue, _ToValue, _Curve, _Type);
        }
        else
            prct = Twining.EaseInOutSine(timer * _Speed, _FromValue, _ToValue, _Type);

        _ValueEvent.Invoke(Mathf.Lerp(_InitialVal, _FinalVal, prct));



    }

    [ContextMenu("Flip Values")]
    public void InvertValues()
    {
        var x = _ToValue;
        _ToValue = _FromValue;
        _FromValue = x;
        timer = 0;
    }

    [ContextMenu("Reset Values")]
    public void ResetValues()
    {
        
        timer = 0;
        prct = _FromValue;
        _ValueEvent.Invoke(Mathf.Lerp(_InitialVal, _FinalVal, prct));
        event1 = false;
        event0 = false;
    }
    
}
