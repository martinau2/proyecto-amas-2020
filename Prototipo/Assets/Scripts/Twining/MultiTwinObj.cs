﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Axis
{
    _X = 0,
    _Y = 1,
    _Z = 2,
}

public class MultiTwinObj : MonoBehaviour
{
    public TwiningTimeType _Type = TwiningTimeType._Yoyo;
    public float _Speed = 1.0f;
    public List<Transform> _Objects = new List<Transform>();
    public bool _X, _Y, _Z;
    public Vector3 _ObjectivePosition;
    public List<Vector3> _InitialPosition = new List<Vector3>();
    public SerializableEvent _Finish;
    bool done;
    public bool _LocalPos;
    float prct;
    float timer;
    [Range(0, 1)] public float _StartValue;
    [Range(0, 1)] public float _FinalValue;



    // Start is called before the first frame update
    void Start()
    {
        foreach (var Object in _Objects)
        {
            _InitialPosition.Add(Object.position);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer += Time.deltaTime;
        prct = Twining.EaseInOutSine(timer * _Speed, _StartValue, _FinalValue, _Type);

        for (int i = 0; i < _Objects.Count; i++)
        {
            // Global Obj Movement Calculations
            var objpos = new Vector3(_X ? _ObjectivePosition.x : _InitialPosition[i].x,
                _Y ? _ObjectivePosition.y : _InitialPosition[i].y,
                _Z ? _ObjectivePosition.z : _InitialPosition[i].z);


            //Local Obj Position Movement Calculations
            var localobjpos = new Vector3();
            if (_X)
            {
                localobjpos = (_ObjectivePosition.x * _Objects[i].transform.right);
            }
            if (_Y)
            {
                localobjpos += (_ObjectivePosition.y * _Objects[i].transform.up);
            }
            if (_Z)
            {
                localobjpos +=  (_ObjectivePosition.z * _Objects[i].transform.right);
            }
            localobjpos += _InitialPosition[i];

            //var localobjpos = new Vector3(_X ? (_InitialPosition[i].x + _ObjectivePosition.x ) : _InitialPosition[i].x, _Y ? (_InitialPosition[i].y + _ObjectivePosition.y ) : _InitialPosition[i].y,
            //    _Z ? (_InitialPosition[i].z + _ObjectivePosition.z ) : _InitialPosition[i].z);


            _Objects[i].position = Vector3.LerpUnclamped(_InitialPosition[i], _LocalPos ? localobjpos : objpos
                , prct);
        }
        if (prct == 1 && !done)
        {
            done = true;
            _Finish?.Raise();
        }
    }

    [ContextMenu("Flip Values")]
    public void InvertValues()
    {
        var x = _FinalValue;
        _FinalValue = _StartValue;
        _StartValue = x;
        timer = 0;
    }

    [ContextMenu("Reset Values")]
    public void ResetValues()
    {
        timer = 0;
        prct = _StartValue;
    }
}
