﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TorrentSpell : Spell
{
    public Collider _TriggerCollider;
    public float _PrepareTime;
    public float _ActiveTime;
    public float _DamageTicksTimer;
    public List<Collider> inside;
    float timealive;
    float ticktimer;


    public override void Destroy()
    {
        Destroy(gameObject);
    }

    public override void SendDamage(DamageReciber reciber)
    {
        reciber.RaiseEvent(_Info);
    }

    
    void Awake()
    {
        _TriggerCollider.enabled = false;
        inside = new List<Collider>();
        ticktimer = _DamageTicksTimer;
    }
    
    void Update()
    {
        ticktimer += Time.deltaTime;
        timealive += Time.deltaTime;
        if (timealive > _PrepareTime + _Info.Lifetime)
        {
            Destroy();
        }
        else if (timealive > _PrepareTime + _ActiveTime)
        {
            _TriggerCollider.enabled = false;
        }
        else if (timealive > _PrepareTime && _TriggerCollider.enabled == false)
        {
            _TriggerCollider.enabled = true;
        }


        if (ticktimer > _DamageTicksTimer)
        {
            foreach (var coll in inside)
            {
                SendDamage(coll.gameObject.GetComponent<DamageReciber>());
            }
            ticktimer = 0;
        }

    }

    private void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                inside.Add(other);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                inside.Remove(other);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                if (inside.Contains(other))
                {
                    return;
                }
                else
                {
                    inside.Add(other);
                }
                
            }
        }
    }

}
