﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionSpell : Spell
{
    public SphereCollider _TriggerCollider;
    public float _PrepareTime;
    public List<Collider> inside;
    public float _ExplosionRadius = 10;

    public override void Destroy()
    {
        Destroy(gameObject);
    }

    public override void SendDamage(DamageReciber reciber)
    {
        reciber.RaiseEvent(_Info);
    }

    void Awake()
    {
        _TriggerCollider.radius = _ExplosionRadius;
        _TriggerCollider.enabled = false;
        inside = new List<Collider>();
        StartCoroutine(Explode());
    }

    private void Start()
    {
        Destroy(gameObject, _Info.Lifetime);
    }


    public IEnumerator Explode()
    {
        yield return new WaitForSeconds(_PrepareTime);
        _TriggerCollider.enabled = true;
        yield return null;
        foreach (var coll in inside)
        {
            SendDamage(coll.gameObject.GetComponent<DamageReciber>());
        }
        yield return null;
        _TriggerCollider.enabled = false;
    }



    private void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                inside.Add(other);
            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                inside.Remove(other);
            }
        }
    }

    private void OnTriggerStay(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                if (inside.Contains(other))
                {
                    return;
                }
                else
                {
                    inside.Add(other);
                }

            }
        }
    }
}
