﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BallSpell : Spell
{
    Rigidbody rb;
    public ParticleSystem _HitVFX;
    public ParticleSystem _BallVFX;
    public float _DamageRadius;

    Vector3 pos;

    public float _LaunchForce;
    public Vector3 _LaunchDirection;

    public override void Destroy()
    {
        Destroy(gameObject, _Info.Lifetime);
    }

    public override void SendDamage(DamageReciber reciber)
    {
        reciber.RaiseEvent(_Info);
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        pos = transform.position;
        

        
        rb.AddForce(_LaunchDirection * _LaunchForce, ForceMode.Impulse);
    }

    

    
    

    private void OnTriggerEnter(Collider other)
    {

        //Change to instantiate explosions

        if (((1 << other.gameObject.layer) & _DamageLayer) != 0 || ((1 << other.gameObject.layer) & _ColissionLayer) != 0)
        {
            _HitVFX.Play();
            _BallVFX.gameObject.SetActive(false);

            Destroy(gameObject, 4f);

            rb.isKinematic = true;
            rb.velocity = Vector3.zero;
            var colls = Physics.OverlapSphere(transform.position, _DamageRadius, _DamageLayer);
            foreach (Collider collider in colls)
            {
                if (collider.gameObject.GetComponent<DamageReciber>() != null)
                {
                    Debug.Log(collider.name);
                    SendDamage(collider.gameObject.GetComponent<DamageReciber>());
                }
            }

           

            this.enabled = false;

            
        }
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, _DamageRadius);
    }
}



public static class ArcHelper
{
    //Returns Final position OF Projectile based on time
    public static Vector3 PlotTrajectoryAtTime(this Vector3 start, Vector3 startVelocity, float time)
    {

        Vector3 finalpos = start + (startVelocity * time) + (Physics.gravity * (time * time * 0.5f));
        

        return finalpos;
    }

    //returns Start Velocity Of Proyectile based on time
    public static Vector3 GetStartVelocity(this Vector3 start, Vector3 finalpos, float time)
    {
        
        Vector3 startvel = (start + (Physics.gravity * (time * time * 0.5f)) - finalpos) / time;
        startvel *= -1;

        return startvel;

    }


    public static void PlotTrajectory(this Vector3 start, Vector3 startVelocity, float timestep, float maxTime,LayerMask _ColissionLayer)
    {
        Vector3 prev = start;
        for (int i = 1; ; i++)
        {
            float t = timestep * i;
            if (t > maxTime) break;
            Vector3 pos = PlotTrajectoryAtTime(start, startVelocity, t);

            RaycastHit info;

            if (Physics.Raycast(prev, pos - prev, out info, Vector3.Distance(prev, pos), _ColissionLayer))
            {

                break;
            }
            Debug.DrawLine(prev, pos, Color.red);
            prev = pos;
        }
    }


    public static RaycastHit ReturnFinalPos(this Vector3 start, Vector3 startVelocity, float timestep, float maxTime, LayerMask _ColissionLayer)
    {
        RaycastHit info = new RaycastHit();
        Vector3 prev = start;
        for (int i = 1; ; i++)
        {
            float t = timestep * i;
            if (t > maxTime) break;
            Vector3 pos = PlotTrajectoryAtTime(start, startVelocity, t);
            

            if (Physics.Raycast(prev, pos - prev, out info, Vector3.Distance(prev, pos), _ColissionLayer))
            {
                return info;
            } 
            prev = pos;
        }
        return info;
    }
}
