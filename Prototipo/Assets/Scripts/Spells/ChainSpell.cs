﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChainSpell : Spell
{

    public ParticleSystem _HitVFX;
    public float _JumpRadius;
    bool jumping;
    float nojumptimer;

    public Vector3 _Targetpos;
    GameObject target;
    public float _JumpDuration;
    public int _AmountOfJumps;
    int currentjumps;


    [Range(0, 30)] public int minstep;
    [Range(0, 30)] public int maxstep;
    public LineRenderer lr;

    private void Start()
    {
        

        StartCoroutine(StartJumping());
    }

    public IEnumerator StartJumping()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit, 50, _ColissionLayer))
        {
            yield return StartCoroutine(JumpToTarget(hit.point));
            if (hit.collider.GetComponent<DamageReciber>() != null)
            {
                SendDamage(hit.collider.GetComponent<DamageReciber>());
            }
        }
        StartCoroutine(TargetSearchLoop());
    }

    public IEnumerator JumpToTarget(Vector3 target)
    {
        jumping = true;
        _Targetpos = target;
        yield return StartCoroutine(endtoend(_JumpDuration));

        //VFX CALCULATIONS
        _HitVFX.transform.position = _Targetpos;
        _HitVFX.transform.forward = (transform.position - _Targetpos).normalized;
        _HitVFX.Play();


        yield return StartCoroutine(starttoend(_JumpDuration));
        transform.position = _Targetpos;
        jumping = false;
    }

    public IEnumerator TargetSearchLoop()
    {
        while (true)
        {

            if (jumping)
            {
                Debug.Log("Jumping");
                nojumptimer = 0;
                yield return null;
            }
            

            nojumptimer += Time.deltaTime;
            if (nojumptimer > 2f || currentjumps == _AmountOfJumps)
            {
                //Debug.Log("No Jump Target Found Or Reached Max Jumps : Self Destroying");
                this.Destroy();
            }



            var objs = Physics.OverlapSphere(transform.position, _JumpRadius, _DamageLayer);
            foreach (var objective in objs)
            {
                if (target == objective.gameObject) { continue; }


                //Test for no wall transpasing
                //RaycastHit hit;
                //if (Physics.Raycast(transform.position, objective.transform.position,out hit, 2, _ColissionLayer))
                //{
                //    Debug.DrawLine(transform.position, objective.transform.position, Color.red, 2);
                    
                //    Debug.Log("HITWALL");
                //    continue;
                //}


                target = objective.gameObject;
                yield return StartCoroutine(JumpToTarget(objective.transform.position));
                SendDamage(objective.GetComponent<DamageReciber>());
                currentjumps += 1;
                break;
            }
            

            yield return null;
        }
    }


    void Update()
    {
        var maxsteps = 31 - (30 - maxstep);
        var startvel = ArcHelper.GetStartVelocity(transform.position, _Targetpos, 1);



        lr.positionCount = maxsteps;

        for (int i = 0; i < maxsteps; i++)
        {
            var j = i + minstep;
            j = Mathf.Clamp(j, 0, maxsteps);
            var step = (1f / 30f) * j;

            var x = ArcHelper.PlotTrajectoryAtTime(transform.position, startvel, step);

            lr.SetPosition(i, x);
        }
        
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.DrawWireSphere(transform.position, _JumpRadius);
    }


    public override void Destroy()
    {
        Destroy(gameObject, _Info.Lifetime);
    }

    public override void SendDamage(DamageReciber reciber)
    {
        reciber.RaiseEvent(_Info);
    }



    public IEnumerator starttoend(float time)
    {
        var timer = time;
        while (timer > 0)
        {
            var x = Mathf.Lerp(0, 30, 1 - (timer / time));
            x = Mathf.Ceil(x);
            minstep = (int)x;
            timer -= Time.deltaTime;
            yield return null;

        }
    }

    public IEnumerator endtoend(float time)
    {
        maxstep = 0;
        var timer = time;
        while (timer > 0)
        {
            var x = Mathf.Lerp(0, 30, 1 - (timer / time));
            x = Mathf.Ceil(x);
            maxstep = (int)x;
            timer -= Time.deltaTime;
            yield return null;

        }
    }

}
