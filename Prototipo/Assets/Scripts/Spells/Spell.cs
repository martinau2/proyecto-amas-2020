﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct SpellInfo
{
    public MagicType Type;
    public float Damage;
    public float Lifetime;
}

public abstract class Spell : MonoBehaviour
{
    public LayerMask _ColissionLayer;
    public LayerMask _DamageLayer;
    public SpellInfo _Info;

    public abstract void SendDamage(DamageReciber reciber);
    public void SetUp(SpellInfo info)
    {
        _Info = info;
    }
    public abstract void Destroy();

}
