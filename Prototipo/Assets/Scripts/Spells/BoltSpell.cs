﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BoltSpell : Spell
{
    Rigidbody rb;
    public float _Speed;
    public ParticleSystem _HitVFX;
    public ParticleSystem _BallVFX;

    public override void Destroy()
    {
        Destroy(gameObject,_Info.Lifetime);
    }

    public override void SendDamage(DamageReciber reciber)
    {
        reciber.RaiseEvent(_Info);
    }
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        Destroy();

    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = transform.forward * _Speed;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (((1 << other.gameObject.layer) & _DamageLayer) != 0)
        {
            if (other.gameObject.GetComponent<DamageReciber>() != null)
            {
                SendDamage(other.gameObject.GetComponent<DamageReciber>());
            }
        }
        if (((1 << other.gameObject.layer) & _ColissionLayer) != 0)
        {
            _HitVFX.Play();
            _BallVFX.loop = false;
            Destroy(gameObject,4f);
            this.enabled = false;
            rb.velocity = Vector3.zero;
            rb.isKinematic = true;
        }
    }
}
