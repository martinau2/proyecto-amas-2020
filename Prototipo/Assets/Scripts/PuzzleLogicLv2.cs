﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class PuzzleLogicLv2 : MonoBehaviour
{
    [SerializeField]
    List<GameObject> braseros = new List<GameObject>();

    List<GameObject> group1 = new List<GameObject>();
    List<GameObject> group2 = new List<GameObject>();
    List<GameObject> group3 = new List<GameObject>();
    List<GameObject> group4 = new List<GameObject>();
    List<GameObject> group5 = new List<GameObject>();
    List<GameObject> group6 = new List<GameObject>();
    List<GameObject> group7 = new List<GameObject>();
    List<GameObject> group8 = new List<GameObject>();
    List<GameObject> group9 = new List<GameObject>();

    List<List<GameObject>> separateGroups = new List<List<GameObject>>();

    bool isCompleted = false;
    public UnityEvent part1Completed;

    // Start is called before the first frame update
    void Start()
    {
        group1.Add(braseros[0]); group1.Add(braseros[1]); group1.Add(braseros[3]);
        group2.Add(braseros[1]); group2.Add(braseros[0]); group2.Add(braseros[4]); group2.Add(braseros[2]);
        group3.Add(braseros[2]); group3.Add(braseros[1]); group3.Add(braseros[5]);
        group4.Add(braseros[3]); group4.Add(braseros[0]); group4.Add(braseros[4]); group4.Add(braseros[6]);
        group5.Add(braseros[4]); group5.Add(braseros[3]); group5.Add(braseros[7]); group5.Add(braseros[5]); group5.Add(braseros[1]);
        group6.Add(braseros[5]); group6.Add(braseros[2]); group6.Add(braseros[4]); group6.Add(braseros[8]);
        group7.Add(braseros[6]); group7.Add(braseros[3]); group7.Add(braseros[7]);
        group8.Add(braseros[7]); group8.Add(braseros[6]); group8.Add(braseros[4]); group8.Add(braseros[8]);
        group9.Add(braseros[8]); group9.Add(braseros[7]); group9.Add(braseros[5]);

        separateGroups.Add(group1);
        separateGroups.Add(group2);
        separateGroups.Add(group3);
        separateGroups.Add(group4);
        separateGroups.Add(group5);
        separateGroups.Add(group6);
        separateGroups.Add(group7);
        separateGroups.Add(group8);
        separateGroups.Add(group9);
        //--------------------------------

    }

    // Update is called once per frame
    void Update()
    {

        if (isCompleted)
        {
            part1Completed.Invoke();
        }
 
    }

    public void BrazierReaction(GameObject brazierPointed)
    {
        List<GameObject> brazierGroup = new List<GameObject>();

        Transform fuegoclick = brazierPointed.transform.GetChild(0);

        if(fuegoclick.gameObject.activeSelf)
        {
            foreach (List<GameObject> group in separateGroups)
            {
                if (group.IndexOf(brazierPointed)==0)
                {
                    brazierGroup.AddRange(group);

                    foreach (GameObject brazier in brazierGroup)
                    {
                        if (brazier.name!=brazierPointed.name)
                        {
                            Transform child = brazier.transform.GetChild(0);
                            
                            if (!child.gameObject.activeSelf)
                            {
                             
                                child.gameObject.SetActive(true);
                                Debug.Log("activó braser adyacente");
                              
                            }
                            else 
                            {
                                child.gameObject.SetActive(false);
                                Debug.Log("debe desactivar braser adyacente");
                            }
                          
                        }
 
                    }

                }

            }
         
        }

        isCompleted = CheckPuzzleCompleted();
            
    }

    public bool CheckPuzzleCompleted()
    {
        int totalEncendidos=0;

        foreach (GameObject brazier in braseros)
        {
            Transform child = brazier.transform.GetChild(0);
            //Debug.Log("hay " + totalEncendidos + "braseros encendidos");

            Debug.Log(brazier.transform.GetChild(0));

            //Debug.Log("EL ESTADO DEL FUEGO ES    "+ child.gameObject.activeSelf);
            if (child.gameObject.activeSelf)
            {
                totalEncendidos +=1;
                Debug.Log("--------   "+brazier.name);
                Debug.Log("hay " + totalEncendidos + "braseros encendidos");
                if (totalEncendidos == 9)
                {
                    Debug.Log("TODOS ENCENDIDOS");
                    return true;
                }
            }
        }
        return false;
    }
}
