﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AudioManager2 : MonoBehaviour
{
    public AudioSource music;

    public Slider volumeSlider;
    public Slider fxVolumeSlider;

    void Start()
    {
        volumeSlider.value = PlayerPrefs.GetFloat("MusicVolume");
        fxVolumeSlider.value = PlayerPrefs.GetFloat("FxVolume");
        
    }

    void Update()
    {
        music.volume = volumeSlider.value;
        
    }

    public void VolumePrefs()
    {
        PlayerPrefs.SetFloat("MusicVolume", music.volume);
        PlayerPrefs.SetFloat("FxVolume", fxVolumeSlider.value);

    }

    public void BackToMainMenu()
    {
        VolumePrefs();
        SceneManager.LoadScene("MainMenu");
    }

    
}