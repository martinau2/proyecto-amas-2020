﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct DataFromTargetPos
{
    public Vector3 position;
    public Vector3 updirection;
    public Vector3 indicatorpos;
    public Vector3 indicatorup;
}
public abstract class Rune : ScriptableObject
{
    public SpellInfo _Info;

    public ParticleSystem _RuneIdleParticle;
    public ParticleSystem _RuneHoldParticle;
    public ParticleSystem _RuneReleaseParticle;

    public GameObject _RuneMagicPrefab;

    public float _RuneIdleParticleTimer, _RuneHoldParticleTimer, _RuneReleaseParticleTimer;
    
    
    public float _ActionCD;
    protected float currentCD;

    public abstract DataFromTargetPos OnHold(Transform hand);
    public abstract GameObject OnRelease(Transform hand,DataFromTargetPos targetpos);
    public abstract void ReduceCD();
    public abstract float GetCD();

}

[Flags]
public enum MagicType
{
    // Decimal     // Binary 
    None = 0,      // 000000  //None   = 0,
    Fire = 1,     // 000001  //Fire  = 1 << 0,
    Ice = 2,      // 000010  //Ice   = 1 << 1,
    Light = 4,       // 000100  //Light    = 1 << 2,
    Electricity = 8,    // 001000  //Electricity = 1 << 3,
    Healing = 16, // 010000 //Healing = 1 << 4,
}
