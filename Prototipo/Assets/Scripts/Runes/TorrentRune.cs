﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Runes/Torrent")]
public class TorrentRune : Rune
{
    public int _Distance;
    public LayerMask _Layers;


    public override DataFromTargetPos OnHold(Transform hand)
    {
        //Initialize Data;
        DataFromTargetPos data;
        data.indicatorup = Vector3.up;
        var handxz = hand.position;
        handxz.y = 0;


        RaycastHit ray = new RaycastHit();
        Vector3 nohit = Vector3.Scale(hand.forward,new Vector3(_Distance,_Distance,_Distance)) + (Vector3.up*2);
        nohit += hand.position;
        //If Hit Calculate Position downwards twoards objective place
        if (Physics.Raycast(hand.position, hand.forward, out ray, _Distance, _Layers))
        {
            var x = ray.point + Vector3.up;
            Debug.DrawLine(hand.position, x,Color.red);
            if (Physics.Raycast(x , Vector3.down, out ray, 100, _Layers))
            {
                Debug.DrawLine(x, ray.point, Color.green);
                data.position = ray.point;
                data.updirection = (x - ray.point).normalized;
                data.indicatorpos = data.position;
                return (data);
            }
            else
            {
                data.position = ray.point;
                data.updirection = (hand.position - ray.point).normalized;
                data.indicatorpos = data.position;
                return (data);
            }
        }//If No hit, search for position downwards towards objective place
        else
        {
            Debug.DrawLine(hand.position, nohit, Color.blue);
            if (Physics.Raycast(nohit, Vector3.down, out ray, 100, _Layers))
            {
                Debug.DrawLine(nohit ,  ray.point , Color.green);
                data.position =  ray.point;
                data.updirection = (nohit - ray.point).normalized;
                data.indicatorpos = data.position;
                return (data);
            }
            Debug.DrawLine(nohit, nohit + Vector3.down*4, Color.cyan);
            //In case of no place found to place spell, return up direction and last known place
            data.position = handxz + nohit;
            data.updirection = Vector3.up;
            data.indicatorpos = data.position;
            return (data);
        }

    }

    public override GameObject OnRelease(Transform hand,DataFromTargetPos targetpos)
    {
        currentCD = _ActionCD;
        var x = (MonoBehaviour.Instantiate(_RuneMagicPrefab, targetpos.position + new Vector3(0,0.1f,0), hand.rotation)).gameObject;
        if (x.GetComponent<TorrentSpell>() != null)
        {
            x.GetComponent<TorrentSpell>().SetUp(_Info);
        }
        x.transform.up = targetpos.updirection;
        x.transform.localRotation = Quaternion.Euler(0, hand.rotation.eulerAngles.y, 0);
        return x;
    }

    public override void ReduceCD()
    {
        currentCD -= Time.deltaTime;
        currentCD = Mathf.Clamp(currentCD, 0, _ActionCD);
    }
    public override float GetCD()
    {
        return currentCD;
    }
}
