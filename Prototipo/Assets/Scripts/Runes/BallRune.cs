﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Runes/Ball")]
public class BallRune : Rune
{
    public LayerMask _Layers;

    public float _LaunchForce;
    Vector3 _LaunchDirection;
    public float _ArcExpoy;

    private void OnEnable()
    {
        currentCD = 0;
    }

    public override float GetCD()
    {
        return currentCD;
    }

    public override void ReduceCD()
    {
        currentCD -= Time.deltaTime;
        currentCD = Mathf.Clamp(currentCD, 0, _ActionCD);
    }

    public override DataFromTargetPos OnHold(Transform hand)
    {
        DataFromTargetPos data;
        data.position = hand.position;
        data.updirection = hand.up;

        _LaunchDirection = hand.forward / _ArcExpoy;
        var x = ArcHelper.ReturnFinalPos(hand.position, _LaunchDirection * _LaunchForce, 0.2f, 10, _Layers);
        data.indicatorpos = x.point;
        data.indicatorup = x.normal;

        return data;
    }

    public override GameObject OnRelease(Transform hand, DataFromTargetPos targetpos)
    {
        currentCD = _ActionCD;
        var x = (MonoBehaviour.Instantiate(_RuneMagicPrefab, targetpos.position + new Vector3(0, 0.1f, 0), hand.rotation)).gameObject;
        if (x.GetComponent<BallSpell>() != null)
        {
            var bs = x.GetComponent<BallSpell>();
            bs.SetUp(_Info);
            bs._LaunchForce = _LaunchForce;

            bs._LaunchDirection = _LaunchDirection;

        }
        return x;
    }
}
