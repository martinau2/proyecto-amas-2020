﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Runes/Explosion")]
public class ExplosionRune : Rune
{
    public float _LifeSpan;
    public LayerMask _Layers;

    GameObject radiussphere;
    public GameObject _RadiusIndicator;

    public override float GetCD()
    {
        return currentCD;
    }

    public override void ReduceCD()
    {
        currentCD -= Time.deltaTime;
        currentCD = Mathf.Clamp(currentCD, 0, _ActionCD);
    }

    public override DataFromTargetPos OnHold(Transform hand)
    {
        if (radiussphere == null)
        {
            radiussphere = Instantiate(_RadiusIndicator, hand.position, Quaternion.identity);
        }
        else
        {
            radiussphere.transform.position = hand.position;
        }
        DataFromTargetPos data;
        data.position = hand.position;
        data.updirection = hand.up;
        data.indicatorpos = Vector3.zero;
        data.indicatorup = Vector3.up;


        return data;
    }

    public override GameObject OnRelease(Transform hand, DataFromTargetPos targetpos)
    {
        if (radiussphere != null)
        {
            Destroy(radiussphere);
            radiussphere = null;
        }
        currentCD = _ActionCD;
        var x = (MonoBehaviour.Instantiate(_RuneMagicPrefab, targetpos.position + new Vector3(0, 0.1f, 0), Quaternion.identity)).gameObject;
        if (x.GetComponent<Spell>() != null)
        {
            x.GetComponent<Spell>().SetUp(_Info);
        }
        return x;
    }
}
