﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Runes/Chain")]
public class ChainRune : Rune
{
    public LayerMask _ImpactLayers;

    public override float GetCD()
    {
        return currentCD;
    }

    public override void ReduceCD()
    {
        currentCD -= Time.deltaTime;
        currentCD = Mathf.Clamp(currentCD, 0, _ActionCD);
    }

    public override DataFromTargetPos OnHold(Transform hand)
    {
        DataFromTargetPos data;
        data.position = hand.position;
        data.updirection = hand.up;

        RaycastHit ray = new RaycastHit();
        if (Physics.Raycast(hand.position, hand.forward, out ray, 100, _ImpactLayers))
        {
            data.indicatorpos = ray.point;
            data.indicatorup = ray.normal;

        }
        else
        {
            data.indicatorpos = Vector3.zero;
            data.indicatorup = Vector3.up;
        }
        return data;
    }

    public override GameObject OnRelease(Transform hand, DataFromTargetPos targetpos)
    {
        currentCD = _ActionCD;
        var x = (MonoBehaviour.Instantiate(_RuneMagicPrefab, targetpos.position + new Vector3(0, 0.1f, 0), hand.rotation)).gameObject;
        if (x.GetComponent<ChainSpell>() != null)
        {
            x.GetComponent<ChainSpell>().SetUp(_Info);
        }
        return x;
    }
}
