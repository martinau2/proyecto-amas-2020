﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsFlagAndBitwise : MonoBehaviour
{
    public State state;
    // Start is called before the first frame update
    void Start()
    {
        state = State.Melee | State.Ice; // == state = (State)(1 << 0 | 1 << 2);
        state = state & ~State.Ice;
        state = state ^ State.Fire;
        print(state);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

[Flags]public enum State
{
    // Decimal     // Binary 
    None = 0,      // 000000  //None   = 0,
    Melee = 1,     // 000001  //Melee  = 1 << 0,
    Fire = 2,      // 000010  //Fire   = 1 << 1,
    Ice = 4,       // 000100  //Ice    = 1 << 2,
    Poison = 8,    // 001000  //Poison = 1 << 3,
}

public static class  enumHelper 
{
    public static MagicType SetFlag(this MagicType a, MagicType b)
    {
        return a | b;
    }

    public static MagicType UnsetFlag(this MagicType a, MagicType b)
    {
        return a & (~b);
    }

    // Works with "None" as well
    public static bool HasFlagx(this MagicType a, MagicType b)
    {
        return (a & b) == b;
    }

    public static MagicType ToogleFlag(this MagicType a, MagicType b)
    {
        return a ^ b;
    }

}
