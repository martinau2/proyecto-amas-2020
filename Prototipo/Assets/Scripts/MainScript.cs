﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainScript : MonoBehaviour
{
    public AudioSource music;
    // Start is called before the first frame update
    void Start()
    {
        music.volume = PlayerPrefs.GetFloat("MusicVolume");
        
    }

    
    public void StartGame()
    {
        SceneManager.LoadScene("PrototypeEntregaFixed");
        Debug.Log("Starting Game");
    }

    public void Credits()
    {
        //SceneManager.LoadScene("Credits");

    }

    public void Exit()
    {
        Application.Quit();
        Debug.Log("Application Exit");
    }

    public void Settings()
    {
        SceneManager.LoadScene("Settings");

    }
    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
        
    }
}
