﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticPlatformPuzzleLv2 : MonoBehaviour
{
    [SerializeField]
    bool puzzlePart1resolved;
    Animation staticPlatformAnim;

    [SerializeField]
    GameObject antorcha;

    // Start is called before the first frame update
    void Start()
    {
        puzzlePart1resolved = false;
        staticPlatformAnim = GetComponent<Animation>();
        staticPlatformAnim.wrapMode = WrapMode.Once;

    }

    // Update is called once per frame
    void Update()
    {
        //if (puzzlePart1resolved == true)
        //{
        //    //transform.position = new Vector3(0f, 0f, 0f);
        //    staticPlatformAnim.Play();
        //    antorcha.SetActive(true);
        //    Debug.Log("plataforma estatica debio moverse");
        //}
        // puzzlePart1resolved = false;

    }

    public void PuzzlePart1Completed()
    {
        puzzlePart1resolved = true;
        staticPlatformAnim.Play();
        antorcha.SetActive(true);
        Debug.Log("plataforma estatica debio moverse");
    }
}
