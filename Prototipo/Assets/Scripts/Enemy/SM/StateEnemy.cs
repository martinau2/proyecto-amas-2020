﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class StateEnemy
{
   public enum STATE
   {
       IDLE, CHASE, PATROL, ATTACK, DIE, HIT

   };

   public enum EVENT
   {
       ENTER, UPDATE, EXIT
   };

   public STATE name;
   protected EVENT stage;
   protected GameObject npc;
   protected Animator anim;
   protected Transform player;
   protected StateEnemy nextState;
   protected NavMeshAgent agent;

   float visDistance = 10.0f;
   float visAngle = 150.0f;
   float visShoot = 4.0f;
   public bool enemyDie = false;
   public bool enemyBeingHit = false;



   public StateEnemy(GameObject _npc, NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
   {
       npc = _npc;
       agent = _navMesh;
       anim = _anim;
       stage = EVENT.ENTER;
       player = _playerPos;
   } 

   public virtual void Enter() {stage = EVENT.UPDATE;}
   public virtual void Update() {stage = EVENT.UPDATE;}
   public virtual void Exit() {stage = EVENT.EXIT;}

   public StateEnemy Process()
   {
       if(stage == EVENT.ENTER) Enter();
       if(stage == EVENT.UPDATE) Update();
       if(stage == EVENT.EXIT)
       {
           Exit();
           return nextState;
       }
       return this;
   }

   public bool CanSeePlayer()
   {
       Vector3 direction = player.position - npc.transform.position;

       float angle = Vector3.Angle(direction, npc.transform.forward);

       if(direction.magnitude <= visDistance && angle<= visAngle)
       {
           return true;
       }

       return false;
   }

   public bool CanAttackPlayer()
   {
       Vector3 direction = player.position - npc.transform.position;

       if(direction.magnitude <= visShoot)
       {
           return true;
       }
       return false;
        
   }
   
   

   public bool EnemyDie()
    {
        
        return true;
    }

    



}
