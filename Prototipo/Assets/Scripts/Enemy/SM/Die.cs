﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Die : StateEnemy
{
    public Die(GameObject _npc, NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
        : base(_npc, _navMesh, _anim, _playerPos)
    {
        name = STATE.DIE;
        
    }

    public override void Enter()
    {

        anim.SetTrigger("isDying");
        

    }

    public override void Update()
    {
        anim.SetTrigger("isDying");
        Debug.Log("Tiene que morir");
        agent.isStopped = true;
        
    }

   
}
