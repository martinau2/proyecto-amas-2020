﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Chase : StateEnemy
{
    public Chase(GameObject _npc, NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
        :base(_npc, _navMesh, _anim, _playerPos)
        {
            name = STATE.CHASE;
            agent.speed = 5f;
            agent.isStopped = false;
        }
    
    public override void Enter()
    {
        anim.SetTrigger("isRunning");
        base.Enter();
    }

    public override void Update()
    {
        agent.SetDestination(player.position);

        if (agent.hasPath)
        {
            if (CanAttackPlayer())
            {
                nextState = new Attack(npc, agent, anim, player);
                stage = EVENT.EXIT;
            }

            else if (!CanSeePlayer())
            {
                nextState = new Patrol(npc, agent, anim, player);
                stage = EVENT.EXIT;
            }

        }

        if (enemyBeingHit)
        {
            Debug.Log("Debe pasar a HIT");
            nextState = new Hit(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }
        if (enemyDie)
        {
            nextState = new Die(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }
    }

    public override void Exit()
    {
        anim.ResetTrigger("isRunning");
        base.Exit();
    }


}
