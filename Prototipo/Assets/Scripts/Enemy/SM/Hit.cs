﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Hit : StateEnemy
{
    float rotationSpeed = 5.0f;

    public Hit(GameObject _npc, NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
         : base(_npc, _navMesh, _anim, _playerPos)
    {
        name = STATE.HIT;
      
    }

    public override void Enter()
    {
        anim.SetTrigger("isBeingHit");
        Debug.Log("esta siendo golpeado");
        agent.isStopped = true;
        

        base.Enter();
    }

    public override void Update()
    {
        //Vector3 direction = player.position - npc.transform.position;
        //float angle = Vector3.Angle(direction, npc.transform.forward);
        //direction.y = 0;

        //npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation,
        //                                      Quaternion.LookRotation(direction), Time.deltaTime * rotationSpeed);

        if (!CanAttackPlayer())
        {
            nextState = new Chase(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

        if (CanAttackPlayer())
        {
            nextState = new Attack(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

        if (enemyDie)
        {
            nextState = new Die(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

    }

    public override void Exit()
    {
        anim.ResetTrigger("isBeingHit");
       

        base.Exit();
    }

}
