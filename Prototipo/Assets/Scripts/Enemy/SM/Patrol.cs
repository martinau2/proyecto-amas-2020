﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Patrol : StateEnemy
{
    int currentIndex = -1;

    public Patrol(GameObject _npc, UnityEngine.AI.NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
        : base(_npc, _navMesh, _anim, _playerPos)
    {
        name = STATE.PATROL;
        agent.speed = 2;
        agent.isStopped = false;

    }

    public override void Enter()
    {
        currentIndex = 0;
        anim.SetTrigger("isRunning");
        base.Enter();
    }

    public override void Update()
    {
        if (CanSeePlayer())
        {
            nextState = new Chase(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

        if (enemyBeingHit)
        {
            Debug.Log("Debe pasar a HIT");
            nextState = new Hit(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

        //else
        //{
        //    nextState = new Chase(npc, agent, anim, player);
        //    stage = EVENT.EXIT;
        //}
        else if (agent.remainingDistance < 1)
        {
            //if (currenteIndex >= GameEnvironment.Singleton.Checkpoints.Count - 1)
            //{
            //    currenteIndex = 0;

            //}
            //else
            //{
            //    currenteIndex++;
            //}
            //agent.SetDestination(GameEnvironment.Singleton.Checkpoints[currenteIndex].transform.position);

            // LA OTRA FORMA CON NUEVOS CHECKPOINTS

            if (Vector3.Distance(this.npc.gameObject.GetComponent<AI>().checkpointsToPatrol[currentIndex].position,
                this.npc.gameObject.GetComponent<Transform>().transform.position)<10f)
            {
                currentIndex ++;

                if (currentIndex >= this.npc.gameObject.GetComponent<AI>().checkpointsToPatrol.Length)
                {
                    currentIndex = 0;
                }
            }
            
            
            agent.SetDestination(npc.gameObject.GetComponent<AI>().checkpointsToPatrol[currentIndex].position);
            

        }

    }

    public override void Exit()
    {
        anim.ResetTrigger("isRunning");
        base.Exit();
    }
}
