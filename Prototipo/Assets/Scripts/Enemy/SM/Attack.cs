﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Attack : StateEnemy
{
    float rotationSpeed = 5.0f;
    AudioSource shoot;


    public Attack(GameObject _npc, NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
        : base(_npc, _navMesh, _anim, _playerPos)
    {
        name = STATE.ATTACK;
        shoot = _npc.GetComponent<AudioSource>();
    }

    public override void Enter()
    {
        anim.SetTrigger("isAttacking");
        //Debug.Log("debe atacar");
        agent.isStopped = true;
        shoot.Play();
        
        base.Enter();
    }

    public override void Update()
    {
        Vector3 direction = player.position - npc.transform.position;
        float angle = Vector3.Angle(direction, npc.transform.forward);
        direction.y = 0;

        npc.transform.rotation = Quaternion.Slerp(npc.transform.rotation, 
                                              Quaternion.LookRotation(direction), Time.deltaTime*rotationSpeed);

        if (!CanAttackPlayer())
        {
                nextState = new Chase(npc, agent, anim, player);
                stage = EVENT.EXIT;
        }

        if (enemyBeingHit)
        {
            Debug.Log("Debe pasar a HIT");
            nextState = new Hit(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }


        if (enemyDie)
        {
            nextState = new Die(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

    }

    public override void Exit()
    {
        anim.ResetTrigger("isAttacking");
        shoot.Stop();

        base.Exit();
    }

}
