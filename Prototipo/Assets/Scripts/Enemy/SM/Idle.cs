﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Idle : StateEnemy
{
    public Idle (GameObject _npc, NavMeshAgent _navMesh, Animator _anim, Transform _playerPos)
        :base(_npc, _navMesh, _anim, _playerPos)
    {
        name = STATE.IDLE; 
    }

    public override void Enter()
    {
        anim.SetTrigger("isIdle");
        base.Enter();
    
    }

    public override void Update()
    {
        //Debug.Log(CanSeePlayer());
        if (CanSeePlayer())
        {
            nextState = new Chase(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }
        // NO BORRAR
        //else if (Random.Range(0, 100) < 10)
        //{
        //    nextState = new Patrol(npc, agent, anim, player);
        //    stage = EVENT.EXIT;
        //}

        else
        {
            nextState = new Patrol(npc, agent, anim, player);
            stage = EVENT.EXIT;
        }

        //if (EnemyDie())
        //{
        //    nextState = new Die(npc, agent, anim, player);
        //    stage = EVENT.EXIT;
        //}

    }

    public override void Exit()
    {
        anim.ResetTrigger("isIdle");

        base.Exit();
    }
}
