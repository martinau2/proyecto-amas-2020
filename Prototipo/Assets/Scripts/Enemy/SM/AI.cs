﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour
{
    public NavMeshAgent agent;
    Animator anim;
    Enemy enemy;

    Transform player;
    GameObject gPlayer;
   // [HideInInspector]
    public Transform[] checkpointsToPatrol;

    [SerializeField]
    public StateEnemy currentState;

    private void Awake()
    {
        checkpointsToPatrol = new Transform[2];
        gPlayer = GameObject.FindGameObjectWithTag("Player");
        player = gPlayer.transform;
    }


    // Start is called before the first frame update
    void Start()
    {
        agent = this.GetComponent<NavMeshAgent>();
        // empieza prueba
        agent.enabled = false;
        Invoke("EnableNavMeshAgent", 0.025f);
        // termina prueba
        anim = this.GetComponent<Animator>();
        currentState = new Idle(this.gameObject, agent, anim, player);
        enemy = this.GetComponent<Enemy>();
        
    }

    // Update is called once per frame
    void Update()
    {
        currentState = currentState.Process();
    }

    public void EnemyBeignHit()
    {
        //enemy.enemyInfo.Life -= this.GetComponent<DamageReciber>();
    }

    private void EnableNavMeshAgent()
    {
        agent.enabled = true;
    }
}
