﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttackSound : MonoBehaviour
{
    public AudioSource sonido;
    // Start is called before the first frame update
    void Awake()
    {
        sonido = GetComponent<AudioSource>();
        //sonido.volume = PlayerPrefs.GetFloat("FXVolume");

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnGUI()
    {
        sonido.volume = PlayerPrefs.GetFloat("FxVolume");
    }
    public void PlaySound()
    {
        sonido.PlayOneShot(sonido.clip);
    }
}
