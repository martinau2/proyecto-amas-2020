﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeHit : MonoBehaviour
{
    public GameObject player;
    PlayerStats life;
    public Character _Char;
    public PlayerStats _PlayerStats;
    float initialHealth;

    private void Awake()
    {
        //life = player.GetComponent<PlayerStats>();
       
    }

    // Start is called before the first frame update
    void Start()
    {
        
        _PlayerStats = GetComponent<PlayerStats>();
        _Char = GetComponent<Character>();
        //initialHealth = _PlayerStats._Stats.health;
       // Debug.Log(initialHealth);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.name == "CharPlayer")
        {
           // Debug.Log("antes de bajarle la vida: "+ _PlayerStats._Stats.health);
            _PlayerStats.ChangeHP(GetComponentInParent<Emelee>().enemyInfo.Damage*-1);
           // Debug.Log("Le pegue al player");
           // Debug.Log(_PlayerStats._Stats.health);
        }
    }
}
