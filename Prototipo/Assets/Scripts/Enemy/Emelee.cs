﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Emelee : Enemy
{
    public GameObject player;
   // PlayerStats life;

    private void Awake()
    {
        //life = player.GetComponent<PlayerStats>();
    }

    // Start is called before the first frame update
    void Start()
    {
        enemyInfo.Life = 80;
       // Debug.Log("Enemy life = "+ enemyInfo.Life);
        
    }

    // Update is called once per frame
    void Update()
    {
        //Esto podria estar adentro de trigger enter, no es necesario chekear cada frame si murio, mejor chekear enseguida despues de que que le golpeen
        //if (enemyInfo.Life <= 0)
        //{
        //    GetComponent<AI>().currentState.enemyDie = true;
        //}
        
    }
    
    private void OnTriggerEnter(Collider collision)
    {
        Debug.Log("Colisione con " + collision.gameObject.name);
        if (collision.gameObject.layer == 9)
        {
            enemyInfo.Life -= 20;
            Debug.Log("vida enemigo  " + enemyInfo.Life);
            GetComponent<AI>().currentState.enemyBeingHit = true;
            Debug.Log(GetComponent<AI>().currentState.enemyBeingHit);
        }
        if (enemyInfo.Life <= 0)
        {
            GetComponent<AI>().currentState.enemyDie = true;
        }
        //if (collision.gameObject.name == "CharPlayer")
        //{
        //    life.ChangeHP(enemyInfo.Damage*-1);
        //    Debug.Log("Le pegue al player");
        //    Debug.Log(life);
        //}

    }


    //Sample Damage
    public void TakeDamage(SpellInfo info)
    {
        enemyInfo.Life -= info.Damage;
        Debug.Log("vida enemigo  " + enemyInfo.Life);
        GetComponent<AI>().currentState.enemyBeingHit = true;
        Debug.Log(GetComponent<AI>().currentState.enemyBeingHit);

        if (enemyInfo.Life == 0)
        {
            GetComponent<EnemyDissolveController>().Desintegrate();
        }
        
    }   　
}
