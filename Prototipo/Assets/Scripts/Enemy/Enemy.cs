﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct EnemyInfo
{
    public float Life;
    public float Damage;

}
public abstract class Enemy : MonoBehaviour
{
    public EnemyInfo enemyInfo;

   
    private void Update()
    {
        if(enemyInfo.Life<=0)
        {
            GetComponent<AI>().currentState.EnemyDie();
        }
       
    }

}

