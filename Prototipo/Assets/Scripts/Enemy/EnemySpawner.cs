﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    private float nextSpawnTime;

    public GameObject prefab;
    public int cantidadAspawnear;
    public int cantidadTandas;
    public float spawnTime = 3f;

    public List<CheckpointsEnemy> spawnerPoints;
 
   

    private void Update()
    {
        if (ShouldSpawn())
        {
            SpawnEnemy();
        }

    }


    //  NO MODIFICAR
    void SpawnEnemy()
    {
  
        if (cantidadTandas >= 0)
        {
            nextSpawnTime = Time.time + spawnTime;

            foreach (CheckpointsEnemy spawpoint in spawnerPoints)
            {
                GameObject asd = Instantiate(prefab, spawpoint.position.GetComponent<Transform>().transform);

                asd.GetComponent<AI>().checkpointsToPatrol[0] = spawpoint.checkpointSerialized[0].transform;
                asd.GetComponent<AI>().checkpointsToPatrol[1] = spawpoint.checkpointSerialized[1].transform;
            }



            //for (int i = 0; i < spawnerPoints.Count; i++)
            //{
            //    GameObject asd = Instantiate(prefab, spawnerPoints[i].position.GetComponent<Transform>().transform);



            //    asd.GetComponent<AI>().checkpointsToPatrol.Add(spawnerPoints[i].checkpointSerialized[0].transform);
            //    asd.GetComponent<AI>().checkpointsToPatrol.Add(spawnerPoints[i].checkpointSerialized[1].transform);
            //}


            cantidadTandas--;

        }



    }
    
    //  NO MODIFICAR
    private bool ShouldSpawn()
    {
        if (cantidadTandas > 0)
        {
            return Time.time >= nextSpawnTime;
            
        }
        return false;
    }
}
