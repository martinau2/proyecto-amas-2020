﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerEnemies : MonoBehaviour
{
    public UnityEvent triggerEnemies;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            triggerEnemies.Invoke();

        }
    }
}
