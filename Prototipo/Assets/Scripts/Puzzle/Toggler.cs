﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;

public class Toggler : MonoBehaviour
{
    public MagicType _Type;
    public UnityEventBool _OnEvent;
    public UnityEventBool _OffEvent;
    public bool _IsOn = false;
    public bool _Revesable = false;
    public bool _OneTimer = true;
    bool activated = false;
    public Puzzle _HeadPuzzle;
    [Range(1,100)] public int indx;


    private void Start()
    {
        if (_IsOn)
            _OnEvent.Invoke(_IsOn);
        else 
            _OffEvent.Invoke(_IsOn);

        if (_HeadPuzzle.IndxExist(indx))
        {
            _HeadPuzzle.ToggleSwitch(_IsOn, indx);
        }
    }

    public void CheckType(SpellInfo info)
    {
        if (activated) { return; }
        //Check if types in spell or in the toggler match 
        if (info.Type.HasFlag(_Type) || _Type.HasFlagx(info.Type))
        {
            Debug.Log($"Correct Magic Type {info.Type}",gameObject);
            if (_Revesable)
            {
                _IsOn = !_IsOn;
            }
            else
            {
                _IsOn = true;
            }
            if (_OneTimer) { activated = true; }

            if (_IsOn)
                _OnEvent.Invoke(_IsOn);
            else
                _OffEvent.Invoke(_IsOn);

            if (_HeadPuzzle.IndxExist(indx)) {
                _HeadPuzzle.ToggleSwitch(_IsOn, indx);
            }
        }
        else
        {
            Debug.Log($"Wrong Magic Type {info.Type}", gameObject);
        }
    }
    
    
}
