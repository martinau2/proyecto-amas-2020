﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Puzzle : MonoBehaviour
{
    public List<int> _Switches = new List<int>();
    public UnityEventBool _DoneEvent;
    public UnityEventBool _ReverseEvent;
    [HideInInspector]
    public List<GameObject> editoronlygo = new List<GameObject>(100);
    public bool beenactive { get; private set; }

    [ContextMenu("CheckIfDone")]
    private void CheckIfDone()
    {
        for (int i = 0; i < _Switches.Count; i++)
        {
            if (_Switches[i] == 0)
            {
                if (beenactive)
                    _ReverseEvent.Invoke(false);
                return;
            }
        }
        beenactive = true;
        _DoneEvent.Invoke(true);
    }


    public void ToggleSwitch(bool val,int indx)
    {
        _Switches[indx-1] = Convert.ToInt32(val);
        CheckIfDone();
    }


    public bool IndxExist(int indx)
    {
        if (_Switches.Count < indx)
        {
            _Switches.Add(0);
            return IndxExist(indx);
        }
        else
        {
            return true;
        }
    }
}
