﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(CheckpointsEnemy))]
public class CheckpointEnemyEditor: PropertyDrawer
{
    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        // The 6 comes from extra spacing between the fields (2px each)
        return EditorGUIUtility.singleLineHeight * 8 + 6;
    }

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        EditorGUI.LabelField(position, label);

        var positionRect = new Rect(position.x, position.y + 18, position.width, 16);
        var checkpointsRect = new Rect(position.x, position.y + 36, position.width, 16);

        EditorGUI.indentLevel++;

        EditorGUI.PropertyField(positionRect, property.FindPropertyRelative("position"), true);
        EditorGUI.PropertyField(checkpointsRect, property.FindPropertyRelative("checkpointSerialized"),true);
        

        EditorGUI.indentLevel--;

        EditorGUI.EndProperty();
    }

}
