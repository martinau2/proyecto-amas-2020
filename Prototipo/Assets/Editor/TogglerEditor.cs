﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Toggler))]
public class TogglerEditor : Editor
{
    bool gofound;
    

    bool isright;

    public override void OnInspectorGUI()
    {
        gofound = false;
        isright = true;
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);
        serializedObject.Update();
        var tar = target as Toggler;
        DrawPropertiesExcluding(serializedObject, "_IsOn", "_Revesable", "_OneTimer", "activated", "_HeadPuzzle", "indx");

        GUILayout.BeginHorizontal();
        GUILayout.Space(100f);
        GUILayout.Label("Editor For Toggler");
        GUILayout.EndHorizontal();

        //Check Errors
        tar.indx = EditorGUILayout.IntSlider(tar.indx,1,100);
        if (tar._HeadPuzzle != null)
        {
            for (int i = 0; i < tar._HeadPuzzle.editoronlygo.Count; i++)
            {
                if (tar._HeadPuzzle.editoronlygo[i] == tar.gameObject)
                {
                    gofound = true;
                    //Indx matches Head Puzzle Indx
                    if (i + 1 == tar.indx)
                    {
                        StyleofText.normal.textColor = new Color(0.2f, 0.73f, 0.1f);
                        GUILayout.Label("Indx matches Head Puzzle indx", StyleofText);
                        break;
                    }
                    //Indx does not match head puzzle indx
                    StyleofText.normal.textColor = Color.red;
                    GUILayout.Label("ERROR : Index missmatch \n make sure index matches the Element index", StyleofText);
                    isright = false;
                    if (GUILayout.Button("Fix MisMatch?"))
                    {
                        tar.indx = i + 1;
                    }
                }
            }
            //Not in the go list of Head Puzzle
            if (!gofound)
            {
                StyleofText.normal.textColor = Color.red;
                GUILayout.Label("ERROR : Toggler not attach to Head Puzzle", StyleofText);
                isright = false;
                if (GUILayout.Button("Attach to Head Puzzle?"))
                {
                    AttachTogglerToHeadPuzzle(tar);
                }
            }
        }

        GUILayout.BeginHorizontal();
        if (EditorApplication.isPlaying)
        {
            GUILayout.Label($"Is Active? :");
        }
        else
        {
            GUILayout.Label($"Start Active? :");
        }
        GUILayout.Space(-250f);
        tar._IsOn = GUILayout.Toggle(tar._IsOn, "");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label($"Is Reversable? :");
        GUILayout.Space(-250f);
        tar._Revesable = GUILayout.Toggle(tar._Revesable, "");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label($"Work Only Once? :");
        GUILayout.Space(-240f);
        tar._OneTimer = GUILayout.Toggle(tar._OneTimer, "");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Label($"HeadPuzzle :");
        GUILayout.Space(-225f);
        if (tar._HeadPuzzle != null)
        {
            GUILayout.Label($"{tar._HeadPuzzle.name}");
            //tar._HeadPuzzle = EditorGUILayout.ObjectField(tar._HeadPuzzle, typeof(Puzzle)) as Puzzle;
        }
        else
        {
            GUILayout.Label($"Null");
            //tar._HeadPuzzle = EditorGUILayout.ObjectField(new Puzzle(), typeof(Puzzle)) as Puzzle;
            GUILayout.EndHorizontal();
            StyleofText.normal.textColor = Color.red;
            GUILayout.Label("ERROR : Please Set Up HeadPuzzle Below", StyleofText);
            GUILayout.BeginHorizontal();
        }
        
        GUILayout.EndHorizontal();
        if (GUILayout.Button("Change HeadPuzzle"))
        {
            
            ScriptableObject.CreateInstance<SelectHeadPuzzle>().Init(this);
        }


        this.serializedObject.ApplyModifiedProperties();
        //IMplement all the editor ...

    }

    private static void AttachTogglerToHeadPuzzle(Toggler tar)
    {
        for (int i = 0; i < tar._HeadPuzzle.editoronlygo.Count; i++)
        {
            if (tar._HeadPuzzle.editoronlygo[i] == null)
            {
                tar._HeadPuzzle.editoronlygo[i] = tar.gameObject;
                break;
            }
        }
    }

    public void SetUpHeadPuzzle(Puzzle puzz)
    {
        var tar = target as Toggler;
        if (tar._HeadPuzzle != null)
            tar._HeadPuzzle.editoronlygo.Remove(tar.gameObject);

        tar._HeadPuzzle = puzz;
        if (puzz != null)
        {
            AttachTogglerToHeadPuzzle(tar);
        }

    }

    void OnSceneGUI()
    {
        var tar = target as Toggler;
        if (tar._HeadPuzzle == null) { return; }

        //isright = tar._HeadPuzzle;
        //isright = tar._HeadPuzzle.editoronlygo.Contains(tar.gameObject);

        Handles.color = isright ? Color.green : Color.red;

        Handles.DrawDottedLine(tar.transform.position,tar._HeadPuzzle.transform.position,3);
        
    }

    

}

public class SelectHeadPuzzle : EditorWindow
{
    public TogglerEditor editorx;
    
    public void Init(TogglerEditor editor)
    {
        editorx = editor;
        this.position = new Rect(Screen.width / 2, Screen.height / 2, 250, 350);
        this.ShowPopup();
        

    }

    void OnGUI()
    {
        if (editorx == null) { this.Close(); }
        GUILayout.BeginHorizontal();
        GUILayout.Space(20f);
        EditorGUILayout.LabelField("Select available HeadPuzzle below", EditorStyles.wordWrappedLabel);
        GUILayout.EndHorizontal();

        Puzzle[] list = FindObjectsOfType<Puzzle>();
        foreach (var puzz in list)
        {
            if (GUILayout.Button(puzz.name)) { editorx.SetUpHeadPuzzle(puzz); this.Close(); }
        }
        if (GUILayout.Button("Set Null")) { editorx.SetUpHeadPuzzle(null); this.Close(); }
    }

    private void OnDestroy()
    {
        
    }
}
