﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
[CustomEditor(typeof(Puzzle))]
public class PuzzleEditor : Editor
{
    string x;
    bool isright;

    public override void OnInspectorGUI()
    {
        GUIStyle StyleofText = new GUIStyle(EditorStyles.label);
        serializedObject.Update();
        var tar = target as Puzzle;
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();
        GUILayout.Space(100f);
        GUILayout.Label("Visual Helper For Switches");
        GUILayout.EndHorizontal();
        tar.editoronlygo.Capacity = 100;

        for (int i = 0; i < tar._Switches.Count; i++)
        {
            //Display Gameobject
            GUILayout.BeginHorizontal();
            StyleofText.normal.textColor = new Color(i*(1.0f/ tar._Switches.Count), 1.0f - (i * (1.0f/ tar._Switches.Count)), 1);
            GUILayout.Label("Element " + (i+1).ToString(), StyleofText);
            GUILayout.Space(50f);
            tar.editoronlygo[i] = EditorGUILayout.ObjectField(tar.editoronlygo[i],typeof(GameObject)) as GameObject;
            GUILayout.EndHorizontal();

            //Display Index and show errors
            GUILayout.BeginHorizontal();
            if (tar.editoronlygo[i] != null && tar.editoronlygo[i].GetComponent<Toggler>() != null)
            {
                int indx = tar.editoronlygo[i].GetComponent<Toggler>().indx;
                GUILayout.Label("This object Index = " + indx.ToString());
                GUILayout.Label(tar.editoronlygo[i].GetComponent<Toggler>()._IsOn ? "This Toggler is On" : "This Toggler is Off");
                GUILayout.EndHorizontal();
                
                GUILayout.Label($"This Toggler Magic Requirements = {tar.editoronlygo[i].GetComponent<Toggler>()._Type}");

                if (i + 1 != indx)
                {
                    //Error in case of index miss match between toggler and puzzle
                    StyleofText.normal.textColor = Color.red;
                    Debug.LogError("Puzzle Editor Error: Indexses Don´t Match", tar.gameObject);
                    GUILayout.Label("ERROR : Index missmatch \n make sure index matches the Element index", StyleofText);
                    if (GUILayout.Button("Fix MisMatch?"))
                    {
                        tar.editoronlygo[i].GetComponent<Toggler>().indx = i + 1;
                    }
                }
                GUILayout.BeginHorizontal();

                if (tar.editoronlygo[i].GetComponent<Toggler>()._HeadPuzzle == null)
                {
                    //Error in case of toggler not having this puzzle as head puzzle
                    StyleofText.normal.textColor = Color.red;
                    Debug.LogError("Puzzle Editor Error: Wrong HeadPuzzler on toggler", tar.editoronlygo[i].gameObject);
                    GUILayout.Label("ERROR : HeadPuzzler Not This \n make sure HeadPuzzle is this Puzzle", StyleofText);
                    if (GUILayout.Button("Fix Error?"))
                    {
                        tar.editoronlygo[i].GetComponent<Toggler>()._HeadPuzzle = tar;
                    }
                }
            }
            else if (tar.editoronlygo[i] != null)
            {
                //Error in case of object in list not containing Toggler Component
                StyleofText.normal.textColor = Color.red;
                Debug.LogError("Puzzle Editor Error: Object Missing Toggler Component", tar.gameObject);
                GUILayout.Label("This object dosn´t include the Toggler Component", StyleofText);
                
                if (GUILayout.Button("Fix Error?"))
                {
                    tar.editoronlygo[i].AddComponent<Toggler>();
                }
            }
            else
            {
                //Error in case of missing object
                StyleofText.normal.textColor = Color.red;
                Debug.LogError("Puzzle Editor Error: Missing Object", tar.gameObject);
                GUILayout.Label("Place Toggler object here", StyleofText);
            }
            GUILayout.EndHorizontal();
            

            serializedObject.ApplyModifiedProperties();
        }
        if (tar.beenactive)
        {
            StyleofText.fontSize = 20;
            StyleofText.normal.textColor = Color.green;
            GUILayout.BeginHorizontal();
            GUILayout.Space(75f);
            GUILayout.Label("This Puzzle Is Activated", StyleofText);
            GUILayout.EndHorizontal();
        }

    }


    void OnSceneGUI()
    {
        var tar = target as Puzzle;

        if (tar.editoronlygo == null)
        {
            return;
        }

        for (int i = 0; i < tar._Switches.Count; i++)
        {
            if (!tar?.editoronlygo[i]) { return; }

            if (tar?.editoronlygo[i].GetComponent<Toggler>() == null)
            {

                isright = false;
            }
            else if (i + 1 != tar.editoronlygo[i].GetComponent<Toggler>().indx)
            {
                isright = false;
            }
            else { isright = true; }


            Handles.color = isright ? Color.green : Color.red;
            Handles.DrawDottedLine(tar.transform.position,tar.editoronlygo[i].transform.position,3);
        }
    }
}
