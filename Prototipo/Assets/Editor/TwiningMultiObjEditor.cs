﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(MultiTwinObj))]
public class TwiningMultiObjEditor : Editor
{

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        var tar = target as MultiTwinObj;

        var spacetocenter = 50f;

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Objects To Twin");
        GUILayout.EndHorizontal();

        var x = 0;
        GUILayout.BeginHorizontal();
        for (int i = 0; i < tar._Objects.Count; i++)
        {
            if (x >= 2)
            {
                GUILayout.EndHorizontal();
                x = 0;
                GUILayout.BeginHorizontal();
            }
            x ++;
            

            if (GUILayout.Button(tar._Objects[i].name))
            {
                Selection.activeGameObject = tar._Objects[i].gameObject;
                SceneView.FrameLastActiveSceneView();
            }
            
        }
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Initial Value");
        tar._StartValue = EditorGUILayout.FloatField(tar._StartValue);
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Final Value");
        GUILayout.Space(4f);
        tar._FinalValue = EditorGUILayout.FloatField(tar._FinalValue);
        GUILayout.EndHorizontal();

        //Axis Management
        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Move On X Axis?");
        GUILayout.Space(-spacetocenter*3);
        tar._X = GUILayout.Toggle(tar._X,"");
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Move On Y Axis?");
        GUILayout.Space(-spacetocenter * 3);
        tar._Y = GUILayout.Toggle(tar._Y, "");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Move On Z Axis?");
        GUILayout.Space(-spacetocenter * 3);
        tar._Z = GUILayout.Toggle(tar._Z, "");
        GUILayout.EndHorizontal();

        //Twining Time Type Management
        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Twining Time Type");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        tar._Type = (TwiningTimeType)EditorGUILayout.EnumPopup(tar._Type,GUILayout.Width(110));
        GUILayout.EndHorizontal();

        //Speed Management
        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Twining Speed");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        tar._Speed =  EditorGUILayout.FloatField(tar._Speed, GUILayout.Width(110));
        GUILayout.EndHorizontal();

        //Objective Position Management
        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Target Twin Position");
        GUILayout.EndHorizontal();


        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        DrawPropertiesExcluding(serializedObject, "_Type", "_Speed", "_Objects", "_X", "_Y", "_Z", "_ObjectivePosition", "_InitialPosition", "m_Script","_LocalPos","_Finish","_StartValue","_FinalValue");
        GUILayout.EndHorizontal();
        tar._ObjectivePosition = EditorGUILayout.Vector3Field("", tar._ObjectivePosition);


        //Is the movement local or world position?
        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Local Movement?");
        GUILayout.Space(-spacetocenter*3);
        tar._LocalPos = GUILayout.Toggle(tar._LocalPos, "");
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        GUILayout.Space(spacetocenter);
        GUILayout.Label("Finished Event");
        GUILayout.Space(-spacetocenter/2 );
        SerializableEvent ev = tar._Finish;
        tar._Finish = EditorGUILayout.ObjectField("", ev, typeof(SerializableEvent), true) as SerializableEvent;
        GUILayout.EndHorizontal();


        


        if (GUILayout.Button("Add Selected Obj"))
        {
            tar._Objects.Add(Selection.activeGameObject.transform);
        }
        if (GUILayout.Button("Remove Selected Obj"))
        {
            foreach (var obj in tar._Objects)
            {
                RemoveAllList();
            }
            
        }
        serializedObject.ApplyModifiedProperties();

        void RemoveAllList()
        {
            foreach (var obj in tar._Objects)
            {
                if (obj == Selection.activeGameObject.transform)
                {
                    tar._Objects.Remove(obj);
                    RemoveAllList();
                }
            }
        }
    }
}
