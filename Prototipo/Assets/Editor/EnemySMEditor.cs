﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(AI))]
[CanEditMultipleObjects]
public class EnemySMEditor : Editor
{
    //SerializedProperty e_EnemyGameObject;
   SerializedProperty e_EnemyCurrentState;

    private void OnEnable()
    {
       e_EnemyCurrentState =serializedObject.FindProperty("currentState");
       
    }
    
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
            
        //EditorGUILayout.LabelField(serializedObject.FindProperty("currentState.name").stringValue);

        AI ai = (AI)target;

        GUILayout.Label("Current State: " + ai.currentState.name.ToString());
 
        serializedObject.ApplyModifiedProperties();
    }
}
