﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/StringListener", 0)]
[System.Serializable]
public class StringListener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public SerializableStringEvent Event;
    public UnityEventString Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(string string_)
    {
        Response.Invoke(string_);
    }
}