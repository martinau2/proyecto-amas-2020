﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/IntListener", 0)]
public class IntListener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public SerializableIntEvent Event;
    public UnityEventInt Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(int dmg)
    {
        Response.Invoke(dmg);
    }
}
