﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/BoolListener", 0)]
[System.Serializable]
public class BoolListener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public ScriptableBoolEvent Event;
    public UnityEventBool Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(bool info)
    {
        Response.Invoke(info);
    }
}
