﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/SpellInfoListener", 0)]
[System.Serializable]
public class SpellInfoListener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public ScriptableSpellInfoEvent Event;
    public UnityEventSpellInfo Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised(SpellInfo info)
    {
        Response.Invoke(info);
    }
}
