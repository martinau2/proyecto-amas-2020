﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[AddComponentMenu("Listeners/Listener", 0)]
public class Listener : MonoBehaviour
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    public SerializableEvent Event;
    public UnityEventNoInput Response;

    private void OnEnable()
    {
        Event.RegisterListener(this);

    }
    private void OnDisable()
    {
        Event.DeRegisterListener(this);
    }
    public void OnEventRaised()
    {
        Response.Invoke();
    }
}
