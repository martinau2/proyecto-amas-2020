﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "BoolEvent", menuName = "Events/BoolEvent")]
public class ScriptableBoolEvent : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    private List<BoolListener> listeners = new List<BoolListener>();

    public void Raise(bool info)
    {

        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(info);

        }
    }
    public void RegisterListener(BoolListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
        else { return; }
    }
    public void DeRegisterListener(BoolListener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
        else { return; }
    }
}
