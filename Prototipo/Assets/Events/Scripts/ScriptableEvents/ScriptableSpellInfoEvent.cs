﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SpellInfoEvent", menuName = "Events/SpellInfoEvent")]
public class ScriptableSpellInfoEvent : ScriptableObject
{
    [SerializeField]
    [TextArea(order = 25)]
    private string Description;
    private List<SpellInfoListener> listeners = new List<SpellInfoListener>();

    public void Raise(SpellInfo info)
    {

        for (int i = listeners.Count - 1; i >= 0; i--)
        {
            listeners[i].OnEventRaised(info);

        }
    }
    public void RegisterListener(SpellInfoListener listener)
    {
        if (!listeners.Contains(listener))
        {
            listeners.Add(listener);
        }
        else { return; }
    }
    public void DeRegisterListener(SpellInfoListener listener)
    {
        if (listeners.Contains(listener))
        {
            listeners.Remove(listener);
        }
        else { return; }
    }
}
