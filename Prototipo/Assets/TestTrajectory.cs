﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class TestTrajectory : MonoBehaviour
{
    public Transform GO2;
    [Range(0, 30)] public int minstep;
    [Range(0, 30)] public int maxstep;
    public LineRenderer lr;
    // Update is called once per frame
    void Update()
    {
        var maxsteps = 31 - (30 - maxstep);
        var startvel = ArcHelper.GetStartVelocity(transform.position, GO2.position, 1);



        lr.positionCount = maxsteps;

        for (int i = 0; i < maxsteps; i++)
        {
            var j = i + minstep;
            j = Mathf.Clamp(j, 0, maxsteps);
            var step = (1f / 30f) * j;

            var x = ArcHelper.PlotTrajectoryAtTime(transform.position, startvel, step);

            lr.SetPosition(i, x);
        }


    }

    [ContextMenu("EndToEnd")]
    private void EndToEnd()
    {
        StartCoroutine(endtoend(0.6f));
    }

    [ContextMenu("StartToEnd")]
    private void StartToEnd()
    {
        StartCoroutine(starttoend(0.6f));
    }

    public IEnumerator starttoend(float time)
    {
        var timer = time;
        while (timer > 0)
        {
            var x = Mathf.Lerp(0, 30, 1 - (timer / time));
            x = Mathf.Ceil(x);
            minstep = (int)x;
            timer -= Time.deltaTime;
            yield return null;

        }
    }

    public IEnumerator endtoend(float time)
    {
        maxstep = 0;
        var timer = time;
        while (timer > 0)
        {
            var x = Mathf.Lerp(0, 30, 1 - (timer / time));
            x = Mathf.Ceil(x);
            maxstep = (int)x;
            timer -= Time.deltaTime;
            yield return null;

        }
    }
}
