%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: LeftArm
  m_Mask: 00000000000000000000000000000000000000000100000000000000010000000000000000000000000000000100000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: LArm
    m_Weight: 1
  - m_Path: LArm/LForearm
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LhandIndex1
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LhandIndex1/LhandIndex2
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LhandIndex1/LhandIndex2/LhandIndex3
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LhandIndex1/LhandIndex2/LhandIndex3/LhandIndex4
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandMiddle1
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandMiddle1/LHandMiddle2
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandMiddle1/LHandMiddle2/LHandMiddle3
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandMiddle1/LHandMiddle2/LHandMiddle3/LHandMiddle4
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandPinky1
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandPinky1/LHandPinky2
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandPinky1/LHandPinky2/LHandPinky3
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandPinky1/LHandPinky2/LHandPinky3/LHandPinky4
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandRing1
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandRing1/LHandRing2
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandRing1/LHandRing2/LHandRing3
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandRing1/LHandRing2/LHandRing3/LHandRing4
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandThumb1
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandThumb1/LHandThumb2
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHandThumb1/LHandThumb2/LHandThumb3
    m_Weight: 1
  - m_Path: LArm/LForearm/LHand/LHRune
    m_Weight: 1
  - m_Path: LArm1
    m_Weight: 1
  - m_Path: RArm
    m_Weight: 0
  - m_Path: RArm/RFoream
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandIndex1
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandIndex1/RHandIndex2
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandIndex1/RHandIndex2/RHandIndex3
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandIndex1/RHandIndex2/RHandIndex3/RHandIndex4
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandMiddle1
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandMiddle1/RHandMiddle2
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandMiddle1/RHandMiddle2/RHandMiddle3
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandMiddle1/RHandMiddle2/RHandMiddle3/RHandMiddle4
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandPinky1
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandPinky1/RHandPinky2
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandPinky1/RHandPinky2/RHandPinky3
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandPinky1/RHandPinky2/RHandPinky3/RHandPinky4
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandRing1
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandRing1/RHandRing2
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandRing1/RHandRing2/RHandRing3
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandRing1/RHandRing2/RHandRing3/RHandRing4
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandThumb1
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandThumb1/RHandThumb2
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHandThumb1/RHandThumb2/RHandThumb3
    m_Weight: 1
  - m_Path: RArm/RFoream/RHand/RHRune
    m_Weight: 1
  - m_Path: RArm1
    m_Weight: 0
