﻿Shader "Unlit/MagicParticleEdge"
{
    Properties
    {
		[Header(Texture)]
        _MainTex ("Texture", 2D) = "white" {}
		_TextureDirection("Texture Direction",vector) = (0,0,0,0)
        _AlphaMask ("Alpha Mask", 2D) = "white" {}
		_AlphaMaskStrenght("Mask Strenght",float) = 1
		_CircleAlpha("Alpha",Range(0,1)) = 0
		[HDR]_CircleColor("Color",color) = (1.0,1.0,1.0,1.0)
		[Space(5)]

		[Header(Noise)]
		_Noise("Distortion Map",2D) = "black" {}
		_NoiseStrenght("Distortion Strenght",Float) = 1
		_NoiseDirection("Distortion Direction",vector) = (0,0,0,0)
		[Space(5)]


		[Header(Edge)]
		[HDR]_EdgeColor("EdgeColor",color) = (1.0,1.0,1.0,1.0)
		[Space(5)]


		[Header(Displacement)]
		_Displacement("Displacement Map",2D) = "black" {}
		_DisplacementSpeed("Displacement Direction",vector) = (0,0,0,0)
		_DisplacementAmount("Displacement Strenght",Float) = 0
		[Space(5)]


		[Header(Blend State)]
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlends("SrcBlend", Float) = 1 //"One"
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlends("DestBlend", Float) = 1 //"Zero"
		[Space(5)]



		[Header(Other)]
		[Enum(UnityEngine.Rendering.CullMode)] _Culls("Cull", Float) = 0 //"Off"
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTests("ZTest", Float) = 4 //"LessEqual"
		[Enum(Off,0,On,1)] _ZWrites("ZWrite", Float) = 0.0 //"Off"
		[Enum(UnityEngine.Rendering.ColorWriteMask)] _ColorWriteMasks("ColorWriteMask", Float) = 15 //"All"
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
		Blend[_SrcBlends][_DstBlends]
		ZTest[_ZTests]
		ZWrite[_ZWrites]
		Cull[_Culls]
		ColorMask[_ColorWriteMasks]

        Pass
        {
            CGPROGRAM
			#pragma target 3.0
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
                float4 EdgeColorIn : TEXCOORD1;
				float4 color : COLOR;
				float4 normal : NORMAL;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float depth : DEPTH;
				float4 EdgeColorIn : TEXCOORD1;
            };

			sampler2D _CameraDepthNormalsTexture;
            sampler2D _MainTex , _AlphaMask , _Noise , _Displacement;
            float4 _MainTex_ST, _Noise_ST , _Displacement_ST, _CircleColor, _EdgeColor, _NoiseDirection, _TextureDirection , _DisplacementSpeed;
			float  _CircleAlpha , _AlphaMaskStrenght , _NoiseStrenght , _DisplacementAmount;

            v2f vert (appdata v)
            {
                v2f o;

				o.EdgeColorIn = v.EdgeColorIn;

				float timer = _Time.y / 10;

				float4 timeduv = v.uv;

				timeduv.x += _DisplacementSpeed.x * timer;
				timeduv.y += _DisplacementSpeed.y * timer;

				timeduv.xy *= _Displacement_ST.xy;

				v.vertex += _DisplacementAmount * v.normal  * (((tex2Dlod(_Displacement, timeduv))*2)-1);


                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				//Custom Vertex Data;
				o.uv.zw = v.uv.zw;
				//End
                UNITY_TRANSFER_FOG(o,o.vertex);
				o.color = v.color;

				o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z *_ProjectionParams.w;


                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture
				float2 screenuv = i.vertex.xy / _ScreenParams.xy;

				float screenDepth = DecodeFloatRG(tex2D(_CameraDepthNormalsTexture, screenuv).zw);
				//float diff = screenDepth - i.depth;
				float diff = screenDepth - Linear01Depth(i.vertex.z);
				float intersect = 0;

				if (diff > 0)
					intersect = 1 - smoothstep(0, _ProjectionParams.w * 0.5, diff);






				float timer = _Time.y/10;

				i.uv.x += _TextureDirection.x * timer;
				i.uv.y += _TextureDirection.y * timer;

				float2 timeduv = float2(_NoiseDirection.x*timer, _NoiseDirection.y*timer);

				fixed4 distortion = ((tex2D(_Noise, (i.uv*_Noise_ST)+ timeduv))) ;

				float mmnoise = (distortion.x - 0.5)*2.0;

				mmnoise *= _NoiseStrenght;

				_EdgeColor = lerp(_EdgeColor,_EdgeColor*i.EdgeColorIn*i.EdgeColorIn.a,i.uv.z);

				fixed4 alphamask = tex2D(_AlphaMask, i.uv + mmnoise) * _AlphaMaskStrenght;
                fixed4 col = tex2D(_MainTex, i.uv+mmnoise) * _CircleColor * (i.color*i.color.a) * (i.uv.z + pow(_CircleAlpha, 3));
				col = lerp(col, _EdgeColor, intersect);
				col *= col.a;
				if (_AlphaMaskStrenght > 0) 
				{
					return col  * alphamask;
				}
				else 
				{
					return col ;
				}
            }
            ENDCG
        }
    }
}
