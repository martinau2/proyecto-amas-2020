﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/RayOfLight"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
		_Opacity("Opacity" , float) = 1
		_Color ("Color", color) = (1,1,1,1)
		_FresnelColor ("FresnelColor", color) = (1,1,1,1)
		_FresnelMultiplayer("FresnelMultiplayer" , Range(-3,3)) = 0.0001
	}
		SubShader
	{
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		ZWrite On
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float4 normal : NORMAL;

            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float depth : DEPTH;
				float4 scrPos : TEXCOORD1;
				float4 normal : NORMAL;
				float3 viewT : TEXCOORD2;
            };

			sampler2D _CameraDepthNormalsTexture;
            sampler2D _MainTex;
            float4 _MainTex_ST , _Color , _FresnelColor;
			float _Opacity , _FresnelMultiplayer;
			


            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);

				o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z *_ProjectionParams.w;

				o.scrPos = ComputeScreenPos(v.vertex);

				o.viewT = normalize(WorldSpaceViewDir(v.vertex));

				o.normal = normalize(v.normal);

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
				float2 screenuv = i.vertex.xy / _ScreenParams.xy;
				float screenDepth = DecodeFloatRG(tex2D(_CameraDepthNormalsTexture, screenuv).zw);
				float diff = screenDepth - Linear01Depth(i.vertex.z);
				float intersect = 0;

				if (diff > 0)
					intersect = 1 - smoothstep(0, _ProjectionParams.w * 0.5, diff);



				float fresnel = dot(i.normal, i.viewT);
				fresnel = saturate(1-fresnel);
				fresnel = pow(fresnel, _FresnelMultiplayer);
				


				float2 screenPosition = (i.scrPos.xy / i.scrPos.w);

				float invert = 1 - i.depth;
				invert *= 0.5;
				invert = pow(invert, 2);

				float screendep = 1- invert;
				screendep *= 0.1;


				//screendep = lerp(screendep,0,intersect);
				_Color = lerp(_Color, 0, intersect);


				screendep = clamp(screendep, 0.0, 1.0);

				

				screendep = lerp(screendep, 0, i.scrPos.y);

				//screendep = normalize(screendep);

				screendep = screendep * _Opacity;

				screendep *= 1- lerp(0, 5, invert);
				fresnel *= 1 - lerp(0, 5, invert);

				screendep = clamp(screendep,0,1);
				fresnel = clamp(fresnel, 0, 1);

				fixed4 fresnelcolor = _FresnelColor * fresnel;

				return fixed4(_Color.r, _Color.g, _Color.b, screendep) + (fresnelcolor);

            }
            ENDCG
        }
    }

}
