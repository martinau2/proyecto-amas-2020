﻿Shader "Unlit/MagicParticle"
{
    Properties
    {
		[Header(Texture)]
        _MainTex ("Texture", 2D) = "white" {}
		_TextureDirection("Texture Direction",vector) = (0,0,0,0)
        _AlphaMask ("Alpha Mask", 2D) = "white" {}
		_AlphaMaskStrenghtx("Mask Strenght",float) = 1
		_Alphax("Alpha",Range(0,1)) = 1
		[HDR]_Color("Color",color) = (1.0,1.0,1.0,1.0)
		[Space(5)]
		[Header(Noise)]
		_Noise("Distortion Map",2D) = "black" {}
		_NoiseStrenght("Distortion Strenght",Float) = 1
		_NoiseDirection("Distortion Direction",vector) = (0,0,0,0)
		[Space(5)]

		[Header(Blend State)]
		[Enum(UnityEngine.Rendering.BlendMode)] _SrcBlends("SrcBlend", Float) = 1 //"One"
		[Enum(UnityEngine.Rendering.BlendMode)] _DstBlends("DestBlend", Float) = 1 //"Zero"
		[Space(5)]



		[Header(Other)]
		[Enum(UnityEngine.Rendering.CullMode)] _Culls("Cull", Float) = 0 //"Off"
		[Enum(UnityEngine.Rendering.CompareFunction)] _ZTests("ZTest", Float) = 4 //"LessEqual"
		[Enum(Off,0,On,1)] _ZWrites("ZWrite", Float) = 0.0 //"Off"
		[Enum(UnityEngine.Rendering.ColorWriteMask)] _ColorWriteMasks("ColorWriteMask", Float) = 15 //"All"
    }
    SubShader
    {
        Tags {"Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent"}
		Blend[_SrcBlends][_DstBlends]
		ZTest[_ZTests]
		ZWrite[_ZWrites]
		Cull[_Culls]
		ColorMask[_ColorWriteMasks]
		Lighting Off
		Fog { Color(0,0,0,0) }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
			#pragma target 2.0
			#pragma multi_compile_particles

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float4 uv : TEXCOORD0;
				float4 color : COLOR;
            };

            struct v2f
            {
                float4 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
				float4 color : COLOR;
            };

            sampler2D _MainTex , _AlphaMask , _Noise;
            float4 _MainTex_ST, _Noise_ST , _Color , _NoiseDirection, _TextureDirection;
			float  _Alphax , _AlphaMaskStrenghtx , _NoiseStrenght;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.uv, _MainTex);
				//Custom Vertex Data;
				o.uv.zw = v.uv.zw;
				//End
                UNITY_TRANSFER_FOG(o,o.vertex);
				o.color = v.color;
                return o;
            }



            fixed4 frag (v2f i) : SV_Target
            {
                // sample the texture


				float timer = _Time.y/10;

				i.uv.x += _TextureDirection.x * timer;
				i.uv.y += _TextureDirection.y * timer;

				float2 timeduv = float2(_NoiseDirection.x*timer, _NoiseDirection.y*timer);

				fixed4 distortion = ((tex2D(_Noise, (i.uv*_Noise_ST)+ timeduv))) ;

				float mmnoise = (distortion.x - 0.5)*2.0;

				mmnoise *= _NoiseStrenght;

				fixed4 alphamask = tex2D(_AlphaMask, i.uv + mmnoise) * _AlphaMaskStrenghtx;

				fixed4 tex = tex2D(_MainTex, i.uv + mmnoise) * alphamask;

                float4 col =  tex * _Color * 2.0;

				col *= _Alphax;
				col *= (i.color*i.color.a);
				col *= tex.a * (_Color.a * i.color.a * 2.0f);

				return col ;
            }
            ENDCG
        }
    }
}
