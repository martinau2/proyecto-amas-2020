﻿Shader "Custom/SurfaceCut"
{
    Properties
    {
		_MainTex("Texture", 2D) = "white" {}
		_NormalMap("NormalTexture", 2D) = "bump" {}
		_MetallicMap("Metallic Map", 2D) = "white" {}
		_NoiseTex("Distortion Texture", 2D) = "white" {}
		_DistortionStr("Distortion Stenght",Float) = 0
		_DisGuide("Dissolve Guide", 2D) = "white" {}
		_DisAmount("Dissolve Prcnt",Range(0,1.1)) = 0
		_DisOutline("Dissolve Outline",Range(0,1)) = 0
		_DisSpeed("Speed OutlineDistortion",Vector) = (0,0,0,0)
		[HDR]_OutlineColor("Outline Color ",color) = (0,0,0,0)
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
		Tags { "RenderType" = "Transparent" "Queue" = "Transparent" }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

		sampler2D _MainTex, _NoiseTex, _DisGuide,_NormalMap,_MetallicMap;
			

        struct Input
        {
            float2 uv_MainTex;
			float2 uv_NormalMap;
			float4 COLOR;
			float3 worldPos;
			float3 worldNormal; INTERNAL_DATA
        };

		float4  _NoiseTex_ST;
		float _DisAmount, _DisOutline, _DistortionStr;
		float4 _DisSpeed, _OutlineColor;
        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {

			//Sample UV for 3d map
			float2 UV;

			float rad = 90 * 3.14159 / 180;


			if (abs(IN.worldNormal.x) > 0.5)
			{
				UV = IN.worldPos.yz; // side
				float2 newuv;
				newuv.x = IN.uv_MainTex.x * cos(rad) - IN.uv_MainTex.y * sin(rad);
				newuv.y = IN.uv_MainTex.y * cos(rad) + IN.uv_MainTex.x * sin(rad);
				UV = newuv;
			}
			else if (abs(IN.worldNormal.z) > 0.5)
			{
				UV = IN.worldPos.xy; // front
			}
			else
			{
				UV = IN.worldPos.xz; // top
			}


			//Distortion

			float2 distscroll = float2(_Time.x*_DisSpeed.x, _Time.x*_DisSpeed.y);



			float2 dist = (tex2D(_NoiseTex, UV * _NoiseTex_ST.xy + distscroll).rg - 0.5f) * 2;

			float disguide = tex2D(_DisGuide, UV *  _NoiseTex_ST.xy + (dist*_DistortionStr));

			half4 col = tex2D(_MainTex, IN.uv_MainTex + (dist*_DistortionStr));

			dist = lerp(0, _DisOutline, dist);


			float disvalue = _DisAmount - dist;

			float disamount = disguide - (_DisAmount)-0.001;

			float failsafe = step(0.001, _DisAmount);

			if (_DisAmount == 0)
			{
				disguide = 1;
			}

			half4 emission = half4(0,0,0,1);
			col *= IN.COLOR;
			emission.rgb *= 1 - (step(disguide, _DisAmount + (_DisOutline + dist.x)));
			emission.rgb += _OutlineColor * (step(disguide, _DisAmount + (_DisOutline + dist.x))*failsafe);

			o.Emission = emission.rgb;

			clip(disamount*failsafe);

            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) ;
            o.Albedo = c.rgb ;

			o.Normal = UnpackNormal(tex2D(_NormalMap, IN.uv_NormalMap));
            o.Metallic = _Metallic * tex2D(_MetallicMap, IN.uv_MainTex);
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}
