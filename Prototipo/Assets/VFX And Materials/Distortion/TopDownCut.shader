﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Unlit/TopDownCut"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _NoiseTex ("Distortion Texture", 2D) = "white" {}
		_DistortionStr("Distortion Stenght",Float) = 0
        _DisGuide ("Dissolve Guide", 2D) = "white" {}
		_DisAmount("Dissolve Prcnt",Range(0,1.1)) = 0
		_DisOutline("Dissolve Outline",Range(0,1)) = 0
		_DisSpeed("Speed OutlineDistortion",Vector) = (0,0,0,0)
		[HDR]_OutlineColor("Outline Color ",color) = (0,0,0,0)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		Cull Off
		Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
				float4 color : COLOR;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                UNITY_FOG_COORDS(1)
                float4 vertex : SV_POSITION;
				float3 normal : TEXCOORD2;
				float3 worldPos : TEXCOORD3;
				float4 color : COLOR;
            };

            sampler2D _MainTex, _NoiseTex, _DisGuide;
            float4 _MainTex_ST, _NoiseTex_ST;
			float _DisAmount, _DisOutline, _DistortionStr;
			float4 _DisSpeed, _OutlineColor;

            v2f vert (appdata v)
            {
                v2f o;
				o.color = v.color;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				o.normal = v.normal;
				o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                UNITY_TRANSFER_FOG(o,o.vertex);
				

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {


                // apply fog
                UNITY_APPLY_FOG(i.fogCoord, col);

				float2 UV;

				float rad = 90 * 3.14159 / 180;


				if (abs(i.normal.x) > 0.5)
				{
					UV = i.worldPos.yz; // side
					float2 newuv;
					newuv.x = UV.x * cos(rad) - UV.y * sin(rad);
					newuv.y = UV.y * cos(rad) + UV.x * sin(rad);
					UV = newuv;
				}
				else if (abs(i.normal.z) > 0.5)
				{
					UV = i.worldPos.xy; // front
				}
				else
				{
					UV = i.worldPos.xz; // top
				}



				float2 distscroll = float2(_Time.x*_DisSpeed.x, _Time.x*_DisSpeed.y);



				float2 dist = (tex2D(_NoiseTex, UV * _NoiseTex_ST.xy + distscroll).rg - 0.5f) * 2;

				float disguide = tex2D(_DisGuide, UV *  _NoiseTex_ST.xy + (dist*_DistortionStr));

                half4 col = tex2D(_MainTex, i.uv+(dist*_DistortionStr));

				dist = lerp(0, _DisOutline, dist);


				float disvalue = _DisAmount -dist;

				float disamount = disguide - (_DisAmount) - 0.001;

				float failsafe = step(0.001, _DisAmount);

				if (_DisAmount == 0) 
				{
					disguide = 1;
				}

				col *= i.color;
				col.rgb *= 1-(step(disguide, _DisAmount + (_DisOutline + dist.x)));
				col.rgb += _OutlineColor *(step(disguide,_DisAmount+ (_DisOutline+dist.x))*failsafe);


				clip(disamount*failsafe);

				

                return col;
            }
            ENDCG
        }
    }
}
