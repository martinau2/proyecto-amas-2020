%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_CorrespondingSourceObject: {fileID: 0}
  m_PrefabInstance: {fileID: 0}
  m_PrefabAsset: {fileID: 0}
  m_Name: RightHand
  m_Mask: 00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: armmesh
    m_Weight: 0
  - m_Path: L_arm
    m_Weight: 0
  - m_Path: L_arm/L_elbow
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_middle1
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_middle1/L_middle2
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_middle1/L_middle2/L_middle3
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_middle1/L_middle2/L_middle3/L_middle4
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_pink1
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_pink1/L_pink2
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_pink1/L_pink2/L_pink3
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_pink1/L_pink2/L_pink3/L_pink4
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_point1
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_point1/L_point2
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_point1/L_point2/L_point3
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_point1/L_point2/L_point3/L_point4
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_ring1
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_ring1/L_ring2
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_ring1/L_ring2/L_ring3
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_ring1/L_ring2/L_ring3/L_ring4
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_thumb1
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_thumb1/L_thumb2
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_thumb1/L_thumb2/L_thumb3
    m_Weight: 0
  - m_Path: L_arm/L_elbow/L_wrist/L_thumb1/L_thumb2/L_thumb3/L_thumb4
    m_Weight: 0
  - m_Path: L_arm_Pole
    m_Weight: 0
  - m_Path: L_wrist_Goal
    m_Weight: 0
  - m_Path: R_arm
    m_Weight: 1
  - m_Path: R_arm/R_elbow
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_middle1
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_middle1/R_middle2
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_middle1/R_middle2/R_middle3
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_middle1/R_middle2/R_middle3/R_middle4
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_pink1
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_pink1/R_pink2
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_pink1/R_pink2/R_pink3
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_pink1/R_pink2/R_pink3/R_pink4
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_point1
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_point1/R_point2
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_point1/R_point2/R_point3
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_point1/R_point2/R_point3/R_point4
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_ring1
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_ring1/R_ring2
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_ring1/R_ring2/R_ring3
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_ring1/R_ring2/R_ring3/R_ring4
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_thumb1
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_thumb1/R_thumb2
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_thumb1/R_thumb2/R_thumb3
    m_Weight: 1
  - m_Path: R_arm/R_elbow/R_wrist/R_thumb1/R_thumb2/R_thumb3/R_thumb4
    m_Weight: 1
  - m_Path: R_arm_Pole
    m_Weight: 1
  - m_Path: R_wrist_Goal
    m_Weight: 1
